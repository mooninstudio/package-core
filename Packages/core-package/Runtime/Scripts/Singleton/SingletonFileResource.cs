﻿using System.IO;
using UnityEngine;

namespace Moonin.Resources
{
    public abstract class SingletonFileResource<TResource>
        where TResource : SingletonFileResource<TResource>, new()
    {
        public static TResource Resource => _resource ??= Load();
        private static string DirectoryPath => Path.Combine(Application.persistentDataPath, DirectoryName);
        private static string FilePath => Path.Combine(DirectoryPath, ResourceName);
        
        private static TResource _resource;
        private static readonly string ResourceName = typeof(TResource).Name;
        private const string DirectoryName = "Resources";
        
        private static TResource Load()
        {
            if (!File.Exists(FilePath)) 
            { 
                Debug.Log($"Loaded new {ResourceName}");
                return new TResource();
            }

            using var streamReader = File.OpenText(FilePath);
            var encrypted = streamReader.ReadToEnd();
            var json = EncryptorDecryptor.EncryptDecrypt(encrypted);
            var resource = JsonUtility.FromJson<TResource>(json);
            Debug.Log($"{ResourceName} loaded from file: " + FilePath);
            return resource;
        }

        public static void Save()
        {
            var json = JsonUtility.ToJson(Resource);
            var encrypted = EncryptorDecryptor.EncryptDecrypt(json);

            if (!Directory.Exists(DirectoryPath)) Directory.CreateDirectory(DirectoryPath);
            
            using var streamWriter = File.CreateText(FilePath);
            streamWriter.Write(encrypted);
            Debug.Log($"{ResourceName} saved!");
        }
    }
}