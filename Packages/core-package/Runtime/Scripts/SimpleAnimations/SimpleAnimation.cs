﻿using System;
using System.Collections;
using Moonin.Utilities;
using UnityEngine;

namespace Moonin.SimpleAnimations
{
    public class SimpleAnimation
    {
        private readonly GameObject _animatedObject;
        private AnimationClip _clip;
        private Coroutine _animationCoroutine;
        private float _speedMultiplier;
        private float _tempSpeedMultiplier;

        public SimpleAnimation(GameObject animatedObject)
        {
            _animatedObject = animatedObject;
        }
        public void Play(AnimationClip clip, bool reversed = false, float speedMultiplier = 1, Action onComplete = null)
        {
            Stop();
            SetSpeed(speedMultiplier);
            _animationCoroutine = CoroutineHelpers.Run(PlayAnimationCoroutine(clip, reversed, onComplete: onComplete));
        }
        public void Loop(AnimationClip clip, bool reversed = false, float speedMultiplier = 1, float startProgress = 0, float duration = 0)
        {
            Stop();
            SetSpeed(speedMultiplier);
            _animationCoroutine = CoroutineHelpers.Run(LoopAnimationCoroutine(clip, reversed, startProgress, duration));
        }
        public void Pause()
        {
            _speedMultiplier = 0;
        }
        public void Resume()
        {
            _speedMultiplier = _tempSpeedMultiplier;
        }
        public void Stop()
        {
            CoroutineHelpers.Stop(_animationCoroutine);
        }
        public void SetSpeed(float speedMultiplier)
        {
            _speedMultiplier = speedMultiplier;
            _tempSpeedMultiplier = speedMultiplier;
        }
        public void SampleAnimation(AnimationClip clip, float t, bool reversed = false){
            t = t.Clamp01();
            clip.SampleAnimation(_animatedObject, (reversed ? 1 - t : t) * clip.length);
        }

        private IEnumerator PlayAnimationCoroutine(AnimationClip clip, bool reversed, float? delay = null, Action onComplete = null)
        {
            float progress = 0;
            do
            {
                progress = Mathf.MoveTowards(progress, 1, _speedMultiplier * 1 / clip.length * Time.deltaTime);
                clip.SampleAnimation(_animatedObject, (reversed ? 1 - progress : progress) * clip.length);
                yield return new WaitForEndOfFrame();
            }
            while (progress != 1);
            if (onComplete != null) onComplete.Invoke();
        }
        private IEnumerator LoopAnimationCoroutine(AnimationClip clip, bool reversed, float startProgress = 0, float duration = 0)
        {
            var progress = startProgress;
            var timer = duration;
            while (duration == 0 || timer > 0)
            {
                do
                {
                    clip.SampleAnimation(_animatedObject, (reversed ? 1 - progress : progress) * clip.length);
                    progress = Mathf.MoveTowards(progress, 1, _speedMultiplier * Time.deltaTime);
                    timer = (timer - Time.deltaTime).Min(0);
                    if(duration > 0 && timer == 0)
                        yield break;
                    yield return null;
                }
                while (progress != 1);
                progress = 0;
            }
        }
    }
}
