using UnityEngine;

namespace Moonin.ReactiveProperties
{
    public class FloatProperty : Property<float>
    {
        public FloatProperty(float value = default) : base(value)
        {
        }
        public void Add(float x) => Set(Value + x);
        public void Multiply(float x) => Set(Value * x);
        public void MoveTowards(float target, float step) => Set(Mathf.MoveTowards(Value, target, step));
    }
}