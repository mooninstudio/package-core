﻿using System;
using UnityEngine;

namespace Moonin.ReactiveProperties
{
    [Serializable]
    public class Property<T> : IProperty<T>
    {
        [field: SerializeField] public T Value { get; private set; }

        private event Action<T> Changed;

        public void Set(T value)
        {
            Value = value;
            Changed?.Invoke(value);
        }

        public void AddListener(Action<T> listener, bool repeatLast = false)
        {
            Changed += listener;
            if (repeatLast)
                listener?.Invoke(Value);
        }

        public void RemoveListener(Action<T> listener)
        {
            Changed -= listener;
        }

        public Property(T value = default)
        {
            Value = value;
        }
    }
}