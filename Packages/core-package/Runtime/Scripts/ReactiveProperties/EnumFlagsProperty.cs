using System;

namespace Moonin.ReactiveProperties
{
    public class EnumFlagsProperty<T> : Property<T>
        where T : Enum
    {
        public void Add(T type) => Set(Value.Add(type));
        public void Remove(T type) => Set(Value.Remove(type));
        public void SetFlag(T type, bool set) => Set(set ? Value.Add(type) : Value.Remove(type));
        public bool Is(T type) => Value.Is(type);
        public bool Has(T type) => Value.Has(type);
    }
}