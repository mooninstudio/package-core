﻿using UnityEngine;

namespace Moonin.ReactiveProperties
{
    public class IntProperty : Property<int>
    {
        public IntProperty(int value = default) : base(value)
        {
        }
        public void Add(int x) => Set(Value + x);
        public void MoveTowards(int target, int step) => Set((int) Mathf.MoveTowards(Value, target, step));
    }
}