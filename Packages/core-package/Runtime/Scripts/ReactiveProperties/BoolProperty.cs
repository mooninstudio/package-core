﻿using System;
using UnityEngine;

namespace Moonin.ReactiveProperties
{
    [Serializable]
    public class BoolProperty : IProperty<bool>
    {
        [field:SerializeField] public bool Value { get; private set; }
        
        private event Action<bool> Changed;
        
        public void Set()
        {
            Value = true;
            Changed?.Invoke(Value);
        }
        
        public void Clear()
        {
            Value = false;
            Changed?.Invoke(Value);
        }
        
        public void AddListener(Action<bool> listener, bool repeatLast = false)
        {
            Changed += listener;
            if (repeatLast)
                listener?.Invoke(Value);
        }

        public void RemoveListener(Action<bool> listener)
        {
            Changed -= listener;
        }

        public BoolProperty(bool value = default)
        {
            Value = value;
        }
    }
}