using System;

namespace Moonin.ReactiveProperties
{
    public interface IProperty<out T>
    {
        T Value { get; }
        void AddListener(Action<T> listener, bool repeatLast = false);
        void RemoveListener(Action<T> listener);
    }
}