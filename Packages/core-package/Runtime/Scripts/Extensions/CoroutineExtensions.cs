﻿using System;
using System.Collections;
using Moonin.Utilities;
using UnityEngine;

namespace General.Extensions
{
    public static class CoroutineExtensions
    {
        public static Coroutine Append(this Coroutine target, IEnumerator coroutine)
        {
            return AppendCoroutine(target, coroutine).Run();
        }
        private static IEnumerator AppendCoroutine(Coroutine first, IEnumerator second)
        {
            yield return first;
            yield return second;
        }
        
        public static Coroutine Join(this Coroutine target, IEnumerator coroutine)
        {
            return JoinCoroutine(target, coroutine).Run();
        }
        private static IEnumerator JoinCoroutine(Coroutine a, IEnumerator b)
        {
            var coroutineB = b.Run();
            yield return a;
            yield return coroutineB;
        }
        
        public static IEnumerator Delay(this IEnumerator target, float seconds)
        {
            return DelayCoroutine(target, seconds);
        }
        private static IEnumerator DelayCoroutine(IEnumerator coroutine, float seconds)
        {
            yield return new WaitForSeconds(seconds);
            yield return coroutine;
        }
        
        public static Coroutine OnComplete(this Coroutine target, Action onComplete)
        {
            return OnCompleteCoroutine(target, onComplete).Run();
        }
        private static IEnumerator OnCompleteCoroutine(Coroutine coroutine, Action onComplete)
        {
            yield return coroutine;
            onComplete?.Invoke();
        }
        
        public static IEnumerator OnComplete(this IEnumerator target, Action onComplete)
        {
            return OnCompleteCoroutine(target, onComplete);
        }
        private static IEnumerator OnCompleteCoroutine(IEnumerator coroutine, Action onComplete)
        {
            yield return coroutine;
            onComplete?.Invoke();
        }

        public static IEnumerator Until(this IEnumerator target, IEnumerator coroutine)
        {
            while (coroutine.MoveNext() & target.MoveNext())
            {
                yield return target.Current;
            }
        }
    }
}