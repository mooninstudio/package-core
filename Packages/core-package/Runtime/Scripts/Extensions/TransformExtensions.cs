using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class TransformExtensions
{
    public static void DestroyChildren(this Transform transform)
    {
        foreach (var child in transform.GetDirectChildren())
        {
            Object.Destroy(child.gameObject);
        }
    }
    public static void DestroyChildrenImmediate(this Transform transform)
    {
        foreach (var child in transform.GetDirectChildren())
        {
            Object.DestroyImmediate(child.gameObject);
        }
    }

    public static IEnumerable<Transform> GetDirectChildren(this Transform target, bool includeInactive = true)
    {
        return from object child in target
            select child as Transform
            into childTransform
            where includeInactive || childTransform.gameObject.activeSelf
            select childTransform;
    }
}