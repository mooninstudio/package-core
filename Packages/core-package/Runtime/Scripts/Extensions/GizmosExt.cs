using UnityEngine;

namespace Moonin.Extensions
{
    public static class GizmosExt
    {
        public static void DrawCircle(Vector3 position, Vector3 up, float radius, int resolution = 100)
        {
            var dir = 
                Vector3.RotateTowards(
                    up, 
                    -up,
                    90 * Mathf.Deg2Rad,
                    0)
                    .normalized * radius;
            var step = 360f / (resolution - 1);
            for (var i = 0; i < resolution; i++)
            {
                var nextDir = Quaternion.AngleAxis(step, up) * dir;
                Gizmos.DrawLine(position + dir, position + nextDir);
                dir = nextDir;
            }
        }

        public static void DrawCircle(Vector3 position, float radius, int resolution = 100)
        {
            DrawCircle(position, Quaternion.identity, radius, resolution);
        }
        public static void DrawCircle(Vector3 position, Quaternion rotation, float radius, int resolution = 100)
        {
            var dir = Vector2.left;
            var step = 360f / (resolution - 1);
            for (var i = 0; i < resolution; i++)
            {
                var nextDir = dir.Rotate(step);
                Gizmos.DrawLine(
                    position + rotation * dir * radius, 
                    position + rotation * nextDir * radius);
                dir = nextDir;
            }
        }

        public static void DrawEllipse(Vector3 position, float radius, float tilt, int resolution = 100)
        {
            DrawEllipse(position, Quaternion.identity, radius, tilt, resolution);
        }
        public static void DrawEllipse(Vector3 position, Quaternion rotation, float radius, float tilt, int resolution = 100)
        {
            var dir = Vector2.left;
            var step = 360f / (resolution - 1);
            for (var i = 0; i < resolution; i++)
            {
                var nextDir = dir.Rotate(step);
                Gizmos.DrawLine(
                    position + rotation * dir.Tilt(tilt) * radius, 
                    position + rotation * nextDir.Tilt(tilt) * radius);
                dir = nextDir;
            }
        }
        
        public static void DrawSquare(Vector3 position, Quaternion rotation, Vector2 size)
        {
            var right = rotation * Vector3.right;
            var forward = rotation * Vector3.up;

            var p1 = position - right * .5f * size.x - forward * .5f * size.y;
            var p2 = position - right * .5f * size.x + forward * .5f * size.y;
            var p3 = position + right * .5f * size.x + forward * .5f * size.y;
            var p4 = position + right * .5f * size.x - forward * .5f * size.y;
            
            Gizmos.DrawLine(p1, p2);
            Gizmos.DrawLine(p2, p3);
            Gizmos.DrawLine(p3, p4);
            Gizmos.DrawLine(p4, p1);
        }
    }
}