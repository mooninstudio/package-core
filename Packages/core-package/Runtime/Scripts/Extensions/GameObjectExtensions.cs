﻿using UnityEngine;

public static class GameObjectExtensions
{
    public static void SetLayerWithChildren(this GameObject target, int layerIndex, bool includeInactive = false)
    {
        target.layer = layerIndex;
        Transform[] hierarchy;
        foreach (Transform child in target.transform)
        {
            hierarchy = child.GetComponentsInChildren<Transform>(includeInactive);
            foreach (Transform item in hierarchy) item.gameObject.layer = layerIndex;
        }
    }
    public static bool IsInLayerMask(this GameObject target, int mask)
    {
        return mask == (mask | (1 << target.layer));
    }
}
