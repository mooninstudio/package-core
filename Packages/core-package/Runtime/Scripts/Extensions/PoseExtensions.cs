﻿
using UnityEngine;

namespace DefaultNamespace.Extensions
{
    public static class PoseExtensions
    {
        public static Pose Mirror(this Pose pose)
        {
            var posePosition = pose.position;
            posePosition.x *= -1;
            pose.position = posePosition;

            var poseRotation = pose.rotation;
            poseRotation.y *= -1;
            poseRotation.z *= -1;
            pose.rotation = poseRotation;

            return pose;
        }
    }
}