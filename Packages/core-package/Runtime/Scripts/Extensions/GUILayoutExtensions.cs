﻿using System;
using UnityEngine;

namespace DefaultNamespace.Extensions
{
    public static class GUILayoutExtensions
    {
        public static void Button(string title, Action action, bool enabled = true)
        {
            GUI.enabled = enabled;
            if (GUILayout.Button(title))
                action();
            GUI.enabled = true;
        }

        public static void ButtonWithConfirmation(string title, ref bool isOpen, Action action,
            string confirmText = "Confirm", string cancelText = "Cancel", bool enabled = true)
        {
            var open = isOpen;
            
            if (!open) Button(title, () => open = true, enabled);
            else
            {
                GUILayout.BeginHorizontal();
                Button(confirmText, action);
                Button(cancelText, () => open = false);
                GUILayout.EndHorizontal();
            }

            isOpen = open;
        }
        
        public static void ButtonWithConfirmation(string title, bool isOpen, Action initAction, Action confirmAction, Action cancelAction,
            string confirmText = "Confirm", string cancelText = "Cancel", bool enabled = true)
        {
            
            if (!isOpen) Button(title, initAction, enabled);
            else
            {
                GUILayout.BeginHorizontal();
                Button(confirmText, confirmAction);
                Button(cancelText, cancelAction);
                GUILayout.EndHorizontal();
            }
        }
    }
}