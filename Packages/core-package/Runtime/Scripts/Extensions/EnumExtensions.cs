﻿using System;

public static class EnumExtensions
{
    public static T Next<T>(this T src) where T : Enum
    {
        var arr = (T[])Enum.GetValues(src.GetType());
        var j = Array.IndexOf(arr, src) + 1;
        return (arr.Length == j) ? arr[0] : arr[j];
    }
    public static T Prev<T>(this T src) where T : Enum
    {
        var arr = (T[])Enum.GetValues(src.GetType());
        var j = Array.IndexOf(arr, src) - 1;
        return (-1 == j) ? arr[^1] : arr[j];
    }
    public static T Random<T>() where T : Enum
    {
        T[] values = (T[]) Enum.GetValues(typeof(T));
        return values.RandomElement();
    }
    
    public static bool Has<T>(this Enum type, T value) {
        try {
            return (((int)(object)type & (int)(object)value) == (int)(object)value);
        } 
        catch {
            return false;
        }
    }
    public static bool Is<T>(this Enum type, T value) {
        try {
            return (int)(object)type == (int)(object)value;
        }
        catch {
            return false;
        }    
    }

    public static T Add<T>(this Enum type, T value) {
        try {
            return (T)(object)(((int)(object)type | (int)(object)value));
        }
        catch(Exception ex) {
            throw new ArgumentException(
                $"Could not append value from enumerated type '{typeof(T).Name}'.", ex);
        }    
    }
    public static T Remove<T>(this Enum type, T value) {
        try {
            return (T)(object)(((int)(object)type & ~(int)(object)value));
        }
        catch (Exception ex) {
            throw new ArgumentException(
                $"Could not remove value from enumerated type '{typeof(T).Name}'.", ex);
        }  
    }
}
