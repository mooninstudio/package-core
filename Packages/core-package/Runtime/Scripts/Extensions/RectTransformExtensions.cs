using UnityEngine;

public static class RectTransformExtensions
{
    public static Vector2 ScreenPointToLocalNormalised(this RectTransform rectTransform, Vector2 screenPosition)
	{
		return RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, screenPosition, null, out Vector2 localPos) ?
			Rect.PointToNormalized(rectTransform.rect, localPos) : Vector2.zero;
	}
	public static Vector3[] GetLocalCorners(this RectTransform rectTransform)
	{
        Vector3[] corners = new Vector3[4];
        rectTransform.GetLocalCorners(corners);
        return corners;
    }
	public static Vector2 PointToNormalizedUnclamped(this Rect rect, Vector3 point)
	{
        var pointX = point.x;
        var pointY = point.y;

        var zeroX = rect.center.x - rect.width * .5f;
        var oneX = rect.center.x + rect.width * .5f;
        var zeroY = rect.center.y - rect.height * .5f;
        var oneY = rect.center.y + rect.height * .5f;

        return new Vector2
        {
            x = pointX.MapUnclamped(zeroX, oneX, 0, 1),
            y = pointY.MapUnclamped(zeroY, oneY, 0, 1)
        };
    }
	public static void SetAndStretchToParentSize(this RectTransform rectTransform, RectTransform parent)
    {
        rectTransform.anchoredPosition = parent.position;
        rectTransform.anchorMin = new Vector2(0, 0);
        rectTransform.anchorMax = new Vector2(1, 1);
        rectTransform.pivot = new Vector2(0.5f, 0.5f);
        rectTransform.sizeDelta = parent.rect.size;
        rectTransform.transform.SetParent(parent);
        rectTransform.localScale = Vector3.one;
    }
}