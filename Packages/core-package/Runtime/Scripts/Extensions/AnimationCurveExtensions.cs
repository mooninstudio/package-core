﻿using UnityEngine;

public static class AnimationCurveExtensions
{
    public static float GetTotalTime(this AnimationCurve curve)
    {
        return curve.keys[curve.length - 1].time;
    }
}
