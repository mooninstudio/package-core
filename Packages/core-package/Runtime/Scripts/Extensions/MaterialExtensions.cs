﻿using UnityEngine;

public static class MaterialExtensions
{
    public static bool TrySetColor(this Material material, string name, Color color)
    {
        if (!material.HasProperty(name)) return false;
        material.SetColor(name, color);
        return true;
    }
    public static bool TryGetColor(this Material material, string name, out Color color)
    {
        if (!material.HasProperty(name))
        {
            color = default;
            return false;
        }
        color = material.GetColor(name);
        return true;
    }
}
