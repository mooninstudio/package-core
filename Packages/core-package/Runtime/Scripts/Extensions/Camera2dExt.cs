﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Moonin.Extensions
{
    public static class Camera2dExt
    {
        public static Vector2 PointerWorldPosition()
        {
            var plane = new Plane(Vector3.forward, Vector3.zero);
            var ray = Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());
            plane.Raycast(ray, out var rayDist);
            return ray.GetPoint(rayDist);
        }
    }
}