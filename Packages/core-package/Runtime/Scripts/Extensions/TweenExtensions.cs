using DG.Tweening;
using UnityEngine;

namespace Moonin.DOTween.Extensions
{
    public static class TweenExtensions
    {
        public static Tween AppendOnComplete(this Tween tween, TweenCallback action)
        {
            tween.onComplete += action;
            return tween;
        }

        public static void GotoEnd(this Tween tween)
        {
            tween.Goto(Mathf.Infinity);
        }
    }
}