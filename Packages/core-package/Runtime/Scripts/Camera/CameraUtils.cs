using UnityEngine;

namespace Moonin.Cameras
{
    public static class CameraUtils
    {
        public static float CalculateHeight(float width, float aspect)
        {
            return width / aspect;
        }

        public static float CalculatePerspectiveFoV(float frustumHeight, float distance)
        {
            return 2.0f * Mathf.Atan(frustumHeight * 0.5f / distance) * Mathf.Rad2Deg;
        }

        public static float CalculateWidth(float height, float aspect)
        {
            return height * aspect;
        }
    }
}