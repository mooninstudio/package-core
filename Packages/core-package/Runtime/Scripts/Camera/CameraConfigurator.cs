﻿using System;
using Moonin.Extensions;
using UnityEngine;

namespace Moonin.Cameras
{
    [RequireComponent(typeof(Camera))]
    [ExecuteAlways]
    public class CameraConfigurator : MonoBehaviour
    {
        [SerializeField] private ControlType _controlType;
        [SerializeField] private bool _orthographic;

        [field: SerializeField]
        [field: ConditionalField(nameof(_controlType), new[] {ControlType.Height})]
        public float Height { get; private set; } = 5;

        [field: SerializeField]
        [field: ConditionalField(nameof(_controlType), new[] {ControlType.Width})]
        public float Width { get; private set; } = 5;

        [field: SerializeField]
        [field: ConditionalField(nameof(_controlType), new[] {ControlType.FitRect})]
        public Vector2 FitRect { get; private set; } = Vector2.one;

        [SerializeField]
        [ConditionalField(nameof(_orthographic), false)]
        private float _measureDistance;

        [SerializeField]
        [field: ConditionalField(nameof(_controlType), new[] {ControlType.Height, ControlType.Width})]
        private Vector2 _pivotOffset = new(.5f, .5f);

        private Vector2 PivotOffsetNormalised =>
            _controlType == ControlType.FitRect ? Vector2.zero : _pivotOffset - new Vector2(.5f, .5f);

        private Vector3 PivotOffset => -new Vector3(PivotOffsetNormalised.x * Width, PivotOffsetNormalised.y * Height);

        private Camera _camera;
        private Camera Camera => _camera ??= GetComponent<UnityEngine.Camera>();

        private bool? _orthographicCached;
        private float? _aspectCached;
        private float? _widthCached;
        private float? _heightCached;
        private float? _measureDistanceCached;
        private Vector2? _fitRectCached;
        private ControlType? _controlTypeCached;

        public void SetWidth(float width)
        {
            if (_controlType != ControlType.Width && _controlType != ControlType.FitRect) return;
            Width = width;
            Height = CameraUtils.CalculateHeight(width, Camera.aspect);
            FitRect = new Vector2(Width, FitRect.y);
        }
        public void SetHeight(float height)
        {
            if (_controlType != ControlType.Height && _controlType != ControlType.FitRect) return;
            Height = height;
            Width = CameraUtils.CalculateWidth(Height, Camera.aspect);
            FitRect = new Vector2(FitRect.x, Height);
        }
        public void SetWidthAndHeight(float width, float height)
        {
            switch (_controlType)
            {
                case ControlType.Width:
                    SetWidth(width);
                    return;
                case ControlType.Height:
                    SetHeight(height);
                    return;
                case ControlType.FitRect:
                    FitRect = new Vector2(width, height);
                    if (width / height > Camera.aspect)
                        SetWidth(width);
                    else
                        SetHeight(height);
                    return;
            }
        }

        private void Update()
        {
            if (!CheckDirty()) return;

            if (_controlTypeCached.HasValue && _controlType != _controlTypeCached &&
                _controlType == ControlType.FitRect)
                FitRect = new Vector2(Width, Height);

            if (_controlType == ControlType.FitRect)
                SetWidthAndHeight(FitRect.x, FitRect.y);
            else
                SetWidthAndHeight(Width, Height);

            UpdateCameraSettings();

            ClearDirty();
        }
        private bool CheckDirty()
        {
            //TODO: this doesn't check for any Camera component changes - basically a worse version of OnValidate
            return false
                   || !_orthographicCached.HasValue
                   || !_aspectCached.HasValue
                   || !_widthCached.HasValue
                   || !_heightCached.HasValue
                   || !_controlTypeCached.HasValue
                   || !_measureDistanceCached.HasValue
                   || !_fitRectCached.HasValue
                   || _orthographicCached.Value != _orthographic
                   || _aspectCached.Value != Camera.aspect
                   || _widthCached.Value != Width
                   || _heightCached.Value != Height
                   || _measureDistanceCached.Value != _measureDistance
                   || _fitRectCached.Value != FitRect
                   || _controlTypeCached.Value != _controlType;
        }
        private void ClearDirty()
        {
            _orthographicCached = _orthographic;
            _aspectCached = Camera.aspect;
            _widthCached = Width;
            _heightCached = Height;
            _controlTypeCached = _controlType;
            _fitRectCached = FitRect;
            _measureDistanceCached = _measureDistance;
        }
        private void UpdateCameraSettings()
        {
            if (_orthographic)
            {
                Camera.orthographic = true;
                Camera.orthographicSize = .5f * Height;
            }
            else
            {
                Camera.orthographic = false;
                Camera.fieldOfView = CameraUtils.CalculatePerspectiveFoV(Height, _measureDistance);
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (_controlType == ControlType.None) return;

            Gizmos.color = Color.yellow;
            var trans = transform;
            var transPosition = trans.position;
            var transForward = trans.forward;
            var transUp = trans.up;
            var transRight = trans.right;
            var transRotation = trans.rotation;

            if (!_orthographic)
            {
                var size = new Vector2(Width, Height);
                GizmosExt.DrawSquare(
                    transPosition + transForward * _measureDistance + PivotOffset,
                    transRotation,
                    size);

                var near = Camera.nearClipPlane;
                var nearSize = near / _measureDistance * size;
                GizmosExt.DrawSquare(
                    transPosition + transForward * near + PivotOffset,
                    transRotation,
                    nearSize);

                var far = Camera.farClipPlane;
                var farSize = far / _measureDistance * size;
                GizmosExt.DrawSquare(
                    transPosition + transForward * far + PivotOffset,
                    transRotation,
                    farSize);

                Gizmos.DrawLine(
                    transPosition + nearSize.x * .5f * transRight + nearSize.y * .5f * transUp + near * transForward +
                    PivotOffset,
                    transPosition + farSize.x * .5f * transRight + farSize.y * .5f * transUp + far * transForward +
                    PivotOffset);
                Gizmos.DrawLine(
                    transPosition - nearSize.x * .5f * transRight + nearSize.y * .5f * transUp + near * transForward +
                    PivotOffset,
                    transPosition - farSize.x * .5f * transRight + farSize.y * .5f * transUp + far * transForward +
                    PivotOffset);
                Gizmos.DrawLine(
                    transPosition + nearSize.x * .5f * transRight - nearSize.y * .5f * transUp + near * transForward +
                    PivotOffset,
                    transPosition + farSize.x * .5f * transRight - farSize.y * .5f * transUp + far * transForward +
                    PivotOffset);
                Gizmos.DrawLine(
                    transPosition - nearSize.x * .5f * transRight - nearSize.y * .5f * transUp + near * transForward +
                    PivotOffset,
                    transPosition - farSize.x * .5f * transRight - farSize.y * .5f * transUp + far * transForward +
                    PivotOffset);
            }
            else
            {
                var near = Camera.nearClipPlane;
                var far = Camera.farClipPlane;
                Gizmos.DrawWireCube(
                    transPosition + transForward * Mathf.Lerp(near, far, .5f) + PivotOffset,
                    new Vector3(Width, Height, far - near));
            }

            if (_controlType == ControlType.FitRect)
            {
                Gizmos.color = Color.blue;
                GizmosExt.DrawSquare(
                    transPosition + transForward * (_orthographic ? Camera.nearClipPlane : _measureDistance) +
                    PivotOffset,
                    transRotation,
                    FitRect);
            }
            else
            {
                var pivotDistance = _orthographic ? Camera.nearClipPlane : _measureDistance;
                var pivot = transPosition + transForward * pivotDistance;
                Gizmos.DrawLine(
                    transPosition + Width * .5f * transRight + Height * .5f * transUp + pivotDistance * transForward +
                    PivotOffset, pivot);
                Gizmos.DrawLine(
                    transPosition - Width * .5f * transRight + Height * .5f * transUp + pivotDistance * transForward +
                    PivotOffset, pivot);
                Gizmos.DrawLine(
                    transPosition - Width * .5f * transRight - Height * .5f * transUp + pivotDistance * transForward +
                    PivotOffset, pivot);
                Gizmos.DrawLine(
                    transPosition + Width * .5f * transRight - Height * .5f * transUp + pivotDistance * transForward +
                    PivotOffset, pivot);
            }
        }
        private void OnPreCull()
        {
            transform.position += PivotOffset;
        }
        private void OnPostRender()
        {
            transform.position -= PivotOffset;
        }

        private enum ControlType
        {
            None,
            Width,
            Height,
            FitRect,
        }
    }
}