﻿using UnityEngine;

namespace CoreExtensions
{
    [ExecuteInEditMode]
    public class CameraFovFitter : MonoBehaviour
    {
        private void Update()
        {
            var frustumHeight = 2.0f * transform.localPosition.z * Mathf.Tan(Camera.main.fieldOfView * 0.5f * Mathf.Deg2Rad);
            var frustumWidth = Camera.main.aspect * frustumHeight;

            transform.localScale = new Vector3(frustumWidth, frustumHeight);
        }
    }
}