﻿using System;
using System.Collections;
using UnityEngine;

namespace Moonin.Utilities
{
    public static class CoroutineHelpers
    {
        private static MonoBehaviour mono;
        public static MonoBehaviour Mono => mono ?? CreateCoroutineRunnerTargetObject();

        private static MonoBehaviour CreateCoroutineRunnerTargetObject()
        {
            mono = new GameObject("CoroutineRunner").AddComponent<EmptyMonoBehaviour>();
            return mono;
        }

        public static Coroutine Run(this IEnumerator coroutine)
        {
            return Mono.StartCoroutine(coroutine);
        }

        public static void Stop(this Coroutine coroutine)
        {
            if (coroutine != null) Mono.StopCoroutine(coroutine);
        }

        public static Coroutine DelayFrames(Action action, int delayFrames)
        {
            return DelayFramesCoroutine(action, delayFrames).Run();
        }

        private static IEnumerator DelayFramesCoroutine(Action action, int delayFrames)
        {
            for (var i = 0; i < delayFrames; i++)
                yield return null;
            action();
        }

        public static IEnumerator NewProcessCoroutine(Action<float> action, float duration, bool immediateStart = false,
            Action onComplete = null)
        {
            var progress = 0f;

            if (immediateStart)
                action(progress);

            while (progress != 1)
            {
                progress = Mathf.MoveTowards(progress, 1, Time.deltaTime / duration);
                yield return null;
                action(progress);
            }

            onComplete?.Invoke();
        }

        public static IEnumerator Concat(params IEnumerator[] coroutines)
        {
            foreach (var coroutine in coroutines)
                yield return coroutine;
        }

        public static IEnumerator EveryFrame(Action action, bool skipFrame = false)
        {
            if (skipFrame)
                yield return null;
            while (true)
            {
                action();
                yield return null;
            }
        }
    }
}