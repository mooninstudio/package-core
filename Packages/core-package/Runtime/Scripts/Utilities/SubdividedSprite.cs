using System;
using UnityEngine;

namespace Moonin.Utilities
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class SubdividedSprite : MonoBehaviour
    {
        [SerializeField] private ushort subdivisionsX = 100;
        [SerializeField] private ushort subdivisionsY = 100;
        [SerializeField] private bool drawGizmos;

        private SpriteRenderer spriteRenderer;

        public void Start()
        {
            Subdivide();
        }

        public void OnDrawGizmos()
        {
            if (!drawGizmos)
                return;

            var stepX = 1f / (subdivisionsX + 1);
            var stepY = 1f / (subdivisionsY + 1);
            var countX = subdivisionsX + 2;
            var countY = subdivisionsY + 2;

            var vertices = new Vector2[countX * countY];
            for (var y = 0; y < countY; y++)
            for (var x = 0; x < countX; x++)
                vertices[countX * y + x] = new Vector2(x * stepX, y * stepY);
            for (var i = 0; i < vertices.Length; i++) Gizmos.DrawSphere(vertices[i], .03f);

            var count = (countX - 1) * (countY - 1);
            var triangles = new ushort[count * 6];
            for (var y = 0; y < countY - 1; y++)
            for (var x = 0; x < countX - 1; x++)
            {
                var v = y * countX + x;
                var i = y * (countX - 1) + x;
                triangles[6 * i + 0] = (ushort) v;
                triangles[6 * i + 1] = (ushort) (v + countX);
                triangles[6 * i + 2] = (ushort) (v + 1);
                triangles[6 * i + 3] = (ushort) (v + 1);
                triangles[6 * i + 4] = (ushort) (v + countX);
                triangles[6 * i + 5] = (ushort) (v + countX + 1);
            }

            for (var i = 0; i < triangles.Length; i++)
                if (i != 0)
                    Gizmos.DrawLine(vertices[triangles[i - 1]], vertices[triangles[i]]);
        }

        [Button("Subdivide")]
        public void Subdivide_Btn()
        {
            Subdivide();
        }

        private void Subdivide()
        {
            spriteRenderer = GetComponent<SpriteRenderer>() ?? throw new NullReferenceException(nameof(spriteRenderer));

            var stepX = 1f / (subdivisionsX + 1) * spriteRenderer.sprite.pixelsPerUnit;
            var stepY = 1f / (subdivisionsY + 1) * spriteRenderer.sprite.pixelsPerUnit;
            var countX = subdivisionsX + 2;
            var countY = subdivisionsY + 2;

            var vertices = new Vector2[countX * countY];
            for (var y = 0; y < countY; y++)
            for (var x = 0; x < countX; x++)
                vertices[countX * y + x] = new Vector2(x * stepX, y * stepY);

            var count = (countX - 1) * (countY - 1);
            var triangles = new ushort[count * 6];
            for (var y = 0; y < countY - 1; y++)
            for (var x = 0; x < countX - 1; x++)
            {
                var v = y * countX + x;
                var i = y * (countX - 1) + x;
                triangles[6 * i + 0] = (ushort) v;
                triangles[6 * i + 1] = (ushort) (v + countX);
                triangles[6 * i + 2] = (ushort) (v + 1);
                triangles[6 * i + 3] = (ushort) (v + 1);
                triangles[6 * i + 4] = (ushort) (v + countX);
                triangles[6 * i + 5] = (ushort) (v + countX + 1);
            }

            spriteRenderer.sprite.OverrideGeometry(vertices, triangles);
        }
    }
}