﻿using System;
using UnityEngine;

namespace Moonin.Utilities
{
    public class SerializableDateTime : ISerializationCallbackReceiver
    {
        private string _value;
        public DateTime Value { get; set; }

        public static SerializableDateTime Now => new() {Value = DateTime.Now};

        public void OnBeforeSerialize()
        {
            _value = Value.ToLongDateString();
        }

        public void OnAfterDeserialize()
        {
            Value = DateTime.Parse(_value);
        }
    }
}