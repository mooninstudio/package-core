﻿using System;
using UnityEngine;

namespace Moonin.Utilities.Screenshot
{
    public class Screenshooter : MonoBehaviour
    {
        [SerializeField] private KeyCode snapKey;
        [SerializeField] private int resolutionMultiplier = 1;

#if UNITY_EDITOR
        private void Update()
        {
            if (UnityEngine.Input.GetKeyDown(snapKey))
            {
                var date = DateTime.Now.ToString();
                date = date.Replace("/", "_");
                date = date.Replace(" ", "_");
                date = date.Replace(":", "_");
                date = date.Replace(".", "_");
                var name = $"screenshot_{date}";
                ScreenCapture.CaptureScreenshot($"Screenshots/{name}", resolutionMultiplier);
                print($"Screenshot taken: {name}");
            }
        }
#endif
    }
}