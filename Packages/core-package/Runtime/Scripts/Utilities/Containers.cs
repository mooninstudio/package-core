using System.Collections.Generic;
using UnityEngine;

namespace Moonin.Utilities
{
    public static class Containers
    {
        private static readonly Transform root = new GameObject("Containers").transform;
        private static readonly Dictionary<string, Transform> containers = new();

        public static Transform GetOrCreate(string name)
        {
            if (containers.ContainsKey(name))
                return containers[name];

            var container = new GameObject(name).transform;
            container.SetParent(root);
            containers.Add(name, container);
            return container;
        }

        public static void Destroy(string name)
        {
            if (!containers.ContainsKey(name))
                return;
            
            Object.Destroy(containers[name].gameObject);
            containers.Remove(name);
        }
    }
}