﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Moonin.Tools
{
    public class Explode : MonoBehaviour
    {
        [SerializeField] private float _explosionForce;
        [SerializeField] private float _explosionRadius;
        [SerializeField] private float _explosionUpward;
        [SerializeField] private float _explosionOutward;

        private List<Rigidbody> _rigidbodies;

        private void Start()
        {
            _rigidbodies = GetComponentsInChildren<Rigidbody>().ToList();
            Debug.Log($"Found {_rigidbodies.Count()} parts");
            _rigidbodies.ForEach(rb =>
            {
                var center = rb.worldCenterOfMass;
                var pos = center + Vector3.ProjectOnPlane(transform.position - center, Vector3.up).normalized *
                    _explosionOutward;
                rb.AddExplosionForce(
                    _explosionForce,
                    pos,
                    _explosionRadius,
                    _explosionUpward);
            });
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, _explosionRadius);
        }
    }
}