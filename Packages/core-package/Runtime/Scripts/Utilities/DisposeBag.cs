﻿using System;
using System.Collections.Generic;

namespace Moonin.Utilities
{
    public class DisposeBag : IDisposable
    {
        private readonly List<IDisposable> disposables;

        public DisposeBag()
        {
            disposables = new List<IDisposable>();
        }

        public void Dispose()
        {
            disposables.ForEach(disp => disp.Dispose());
            disposables.Clear();
        }

        public void Add(IDisposable disposable)
        {
            disposables.Add(disposable);
        }

        public void Add(params IDisposable[] disposables)
        {
            this.disposables.AddRange(disposables);
        }

        public void Add(IEnumerable<IDisposable> disposables)
        {
            this.disposables.AddRange(disposables);
        }
    }
}