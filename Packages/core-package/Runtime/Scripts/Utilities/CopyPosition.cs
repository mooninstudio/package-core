﻿using System.Linq;
using UnityEngine;

namespace Moonin.Tools
{
    [ExecuteAlways]
    public class CopyPosition : MonoBehaviour
    {
        [SerializeField] private Transform[] _sources;

        private void Update()
        {
            if (!_sources?.Any() ?? false) return;

            var positions = _sources.Select(x => x.position).ToArray();
            transform.position = new Vector3(
                positions.Average(v => v.x),
                positions.Average(v => v.y),
                positions.Average(v => v.z));
        }
    }
}