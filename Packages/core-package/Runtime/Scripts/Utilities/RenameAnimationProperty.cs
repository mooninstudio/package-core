﻿using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Moonin.Tools
{
    public class RenameAnimationProperty : MonoBehaviour
    {
        [SerializeField] private AnimationClip _clip;
        [SerializeField] private int _trimLength;
        [SerializeField] private string _prefix;
        
        [Button("Read properties")]
        private void ReadProperties()
        {
            if (_clip == null) throw new Exception("No clip assigned!");
            
            var bindings = AnimationUtility.GetCurveBindings(_clip);
            bindings.ForEach((binding, i) => Debug.Log($"{i}: {binding.path}"));
        }
        
        [Button("Trim")]
        private void Trim()
        {
            if (_clip == null) throw new Exception("No clip assigned!");
            
            var bindings = AnimationUtility.GetCurveBindings(_clip).Take(_trimLength).ToArray();
            var curves = bindings.Select(binding => AnimationUtility.GetEditorCurve(_clip, binding)).ToArray();
            AnimationUtility.SetEditorCurves(_clip, bindings, curves);
        }
        
        [Button("Remove prefix")]
        private void RemovePrefix()
        {
            if (_clip == null) throw new Exception("No clip assigned!");
            
            var bindings = AnimationUtility.GetCurveBindings(_clip);
            var curves = bindings.Select(binding => AnimationUtility.GetEditorCurve(_clip, binding)).ToArray();
            bindings = bindings.Select(binding =>
            {
                var path = binding.path.Replace(_prefix, "");
                binding.path = path;
                return binding;
            }).ToArray();
            
            AnimationUtility.SetEditorCurves(_clip, bindings, curves);
        }
        
        [Button("Add prefix")]
        private void AddPrefix()
        {
            if (_clip == null) throw new Exception("No clip assigned!");
            
            var bindings = AnimationUtility.GetCurveBindings(_clip);
            var curves = bindings.Select(binding => AnimationUtility.GetEditorCurve(_clip, binding)).ToArray();
            bindings = bindings.Select(binding =>
            {
                var path = _prefix + binding.path;
                binding.path = path;
                return binding;
            }).ToArray();
            
            AnimationUtility.SetEditorCurves(_clip, bindings, curves);
        }
    }
}