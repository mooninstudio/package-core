using UnityEngine;

namespace Moonin.Utilities
{
    public class FPSLimiter : MonoBehaviour
    {
        [SerializeField] private int maxFPS = 60;

        [ExecuteAlways]
        private void Awake()
        {
#if UNITY_EDITOR
            Application.targetFrameRate = maxFPS;
#endif
        }
    }
}