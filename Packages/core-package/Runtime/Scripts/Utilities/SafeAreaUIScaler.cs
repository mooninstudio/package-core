﻿using UnityEngine;

namespace Moonin.Utilities
{
    [RequireComponent(typeof(Canvas))]
    public class SafeAreaUIScaler : MonoBehaviour
    {
        [SerializeField] private RectTransform safeScreenArea;

        public void Awake()
        {
            var canvas = GetComponent<Canvas>();
            var safeArea = Screen.safeArea;
            var anchorMin = safeArea.position;
            var anchorMax = safeArea.position + safeArea.size;
            anchorMin.x /= canvas.pixelRect.width;
            anchorMin.y /= canvas.pixelRect.height;
            anchorMax.x /= canvas.pixelRect.width;
            anchorMax.y /= canvas.pixelRect.height;

            safeScreenArea.anchorMin = anchorMin;
            safeScreenArea.anchorMax = anchorMax;
            safeScreenArea.sizeDelta = Vector2.zero;
        }
    }
}