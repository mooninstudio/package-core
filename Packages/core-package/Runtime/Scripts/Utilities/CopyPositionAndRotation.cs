﻿using UnityEngine;

namespace Moonin.Tools
{
    [ExecuteAlways]
    public class CopyPositionAndRotation : MonoBehaviour
    {
        [SerializeField] private Transform _source;

        private void Update()
        {
            if (_source == null) return;

            transform.SetPositionAndRotation(_source.position, _source.rotation);
        }
    }
}