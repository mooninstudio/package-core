﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Moonin.Utilities.Collections
{
    public class SerializableHashSet<T> : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private T[] _serializableItems;
        private HashSet<T> _hashSet = new();
 
        void ISerializationCallbackReceiver.OnBeforeSerialize() {
            _serializableItems = _hashSet.ToArray();
            _hashSet = null;
        }
 
        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            _hashSet = new HashSet<T>(_serializableItems);
            _serializableItems = null;
        }
 
        public bool Add(T item) => _hashSet.Add(item);
        public bool Contains(T item) => _hashSet.Contains(item);
    }
}