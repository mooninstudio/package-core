﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Moonin.Utilities.Collections
{
    [Serializable]
    public class SerializableGuidHashSet : IEnumerable<Guid>, ISerializationCallbackReceiver
    {
        [SerializeField] private string[] _serializableItems;
        private HashSet<Guid> _hashSet = new();
 
        void ISerializationCallbackReceiver.OnBeforeSerialize() {
            _serializableItems = _hashSet.Select(x => x.ToString()).ToArray();
        }
 
        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            _hashSet = new HashSet<Guid>(_serializableItems.Select(Guid.Parse));
        }
 
        public bool Add(Guid item) => _hashSet.Add(item);
        public bool Contains(Guid item) => _hashSet.Contains(item);
        public IEnumerator<Guid> GetEnumerator() => _hashSet.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => _hashSet.GetEnumerator();
    }
}