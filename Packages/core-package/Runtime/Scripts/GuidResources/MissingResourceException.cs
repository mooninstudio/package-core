﻿using System;

namespace Moonin.Resources
{
    public class MissingResourceException : Exception
    {
        public MissingResourceException(string resourceName, string path = null) 
            : base($"Resource {resourceName} not found at {(string.IsNullOrEmpty(path) ? $"Resources/{path}" : "Resources")}")
        {
        }
    }
}