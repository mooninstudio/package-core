﻿using UnityEngine;

namespace Moonin.PoseAnimations
{
    public static class PoseFunc
    {
        public static Pose Lerp(Pose from, Pose to, float t)
        {
            return new Pose(
                Vector3.Lerp(from.position, to.position, t),
                Quaternion.Lerp(from.rotation, to.rotation, t));
        }
    }
}