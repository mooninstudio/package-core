﻿using UnityEngine;

namespace Moonin.PoseAnimations
{
    public class AnimationsTester : MonoBehaviour
    {
        [SerializeField] private Armature _target;
        [SerializeField] private Keyframe _frameA;
        [SerializeField] private Keyframe _frameB;
        [SerializeField, Range(0, 1)] private float _time;
        
        [Button]
        private void TakeFrameA() => _frameA = Keyframe.TakeSnapshot(_target);
        [Button]
        private void TakeFrameB() => _frameB = Keyframe.TakeSnapshot(_target);

        private void OnValidate()
        {
            if (_frameA.IsEmpty || _frameB.IsEmpty) return;
            
            KeyframeFunc.LerpApply(_target, _frameA, _frameB, _time);
        }
    }
}