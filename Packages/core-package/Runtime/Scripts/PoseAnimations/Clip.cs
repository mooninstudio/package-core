﻿using System.Collections.Generic;
using UnityEngine;

namespace Moonin.PoseAnimations
{
    public class Clip : ScriptableObject
    {
        [field: SerializeField] public List<Keyframe> Keyframes { get; private set; }
        
        public void Sample(Armature armature, float t)
        {
            switch (Keyframes.Count)
            {
                case 0:
                    break;
                case 1:
                    Keyframes[0].Apply(armature);
                    break;
                default:
                    var timeStep = 1f / (Keyframes.Count - 1);
                    var leftFrame = Mathf.FloorToInt(t.Max(.999f) * (Keyframes.Count - 1));
                    var frameTime = (t - leftFrame * timeStep) / timeStep;
                    
                    KeyframeFunc.LerpApply(armature, Keyframes[leftFrame], Keyframes[leftFrame + 1], frameTime);
                    break;
            }
        }
    }
}