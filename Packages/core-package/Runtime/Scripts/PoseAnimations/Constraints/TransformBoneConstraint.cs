﻿using UnityEditor;
using UnityEngine;

namespace Moonin.PoseAnimations.Constraints
{
    [DisallowMultipleComponent]
    public class TransformBoneConstraint : BoneConstraint
    {
        [SerializeField] private bool _freezePositionX;
        [SerializeField] private bool _freezePositionY;
        [SerializeField] private bool _freezePositionZ;
        [SerializeField] private bool _freezeRotationX;
        [SerializeField] private bool _freezeRotationY;
        [SerializeField] private bool _freezeRotationZ;
        [SerializeField, ReadOnly] private bool _freezeScaleX;
        [SerializeField, ReadOnly] private bool _freezeScaleY;
        [SerializeField, ReadOnly] private bool _freezeScaleZ;

        public override bool Process()
        {
            var boneTransform = Bone.transform;
            var pos = boneTransform.position;
            var rot = boneTransform.rotation;
            var scl = boneTransform.localScale;

            EditorGUI.BeginChangeCheck();
            if (!_freezePositionX) pos = PositionHandle(Vector3.right, Handles.xAxisColor);
            if (!_freezePositionY) pos = PositionHandle(Vector3.up, Handles.yAxisColor);
            if (!_freezePositionZ) pos = PositionHandle(Vector3.forward, Handles.zAxisColor);
            if (!_freezeRotationX) rot = RotationHandle(Vector3.right, Handles.xAxisColor);
            if (!_freezeRotationY) rot = RotationHandle(Vector3.up, Handles.yAxisColor);
            if (!_freezeRotationZ) rot = RotationHandle(Vector3.forward, Handles.zAxisColor);
            // Needs rethinking
            // if (!_freezeScaleX) scl = ScaleHandle(Vector3.right, Handles.xAxisColor);
            // if (!_freezeScaleY) scl = ScaleHandle(Vector3.up, Handles.yAxisColor);
            // if (!_freezeScaleZ) scl = ScaleHandle(Vector3.forward, Handles.zAxisColor);
            if (!EditorGUI.EndChangeCheck()) return false;

            Undo.RecordObject(boneTransform, $"Modified bone {Bone.name} transform");
            boneTransform.position = pos;
            boneTransform.rotation = rot;
            boneTransform.localScale = scl;
            return true;

            Vector3 PositionHandle(Vector3 axis, Color handleColor)
            {
                Handles.color = handleColor;
                return Handles.Slider(pos, rot * axis,
                    HandleUtility.GetHandleSize(pos), Handles.ArrowHandleCap, 0);
            }
            
            Quaternion RotationHandle(Vector3 axis, Color handleColor)
            {
                Handles.color = handleColor;
                return Handles.Disc(rot, pos, rot * axis,
                    HandleUtility.GetHandleSize(pos), true, 0);
            }
            
            Vector3 ScaleHandle(Vector3 axis, Color handleColor)
            {
                Handles.color = handleColor;
                var size = HandleUtility.GetHandleSize(pos);
                var dir = rot * axis;
                return Handles.Slider(pos + dir * size * 1.1f, dir,
                    size * .1f, Handles.CubeHandleCap, 0);
            }
        }
    }
}