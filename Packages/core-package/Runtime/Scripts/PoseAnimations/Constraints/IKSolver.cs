﻿using System.Linq;
using UnityEngine;

namespace Moonin.PoseAnimations.Constraints
{
    public class IKSolver
    {
        private int _armLength;
        private float[] _lengths;
        private float _totalLength;
        private int _iterations;
        private float _accuracy;
        private Transform[] _bones;
        private Vector3[] _positions;
        private Vector3[] _startDirections;
        private Quaternion[] _startRotations;

        public IKSolver(Transform[] bones, int iterations = 10, float accuracy = .001f)
        {
            _bones = bones;
            _iterations = iterations;
            _accuracy = accuracy;
        
            _lengths = _bones.Pairwise().Select(x => Vector3.Distance(x.Item1.position, x.Item2.position)).ToArray();
            _positions = new Vector3[bones.Length];
            _startDirections = _bones.Pairwise().Select(x => (x.Item2.position - x.Item1.position).normalized).ToArray();
            _startRotations = _bones.SkipLast(1).Select(x => x.rotation).ToArray();

            _totalLength = _lengths.Sum();
        }

        public void Solve(Vector3 target, Vector3 pole)
        {
            for (var i = 0; i < _bones.Length; i++)
                _positions[i] = _bones[i].position;
        
            var sqrDistanceToTarget = (target - _positions[0]).sqrMagnitude;

            //  Stretch if target too far
            if (sqrDistanceToTarget >= _totalLength.Sqr())
            {
                // Get the direction towards the target
                var dir = (target - _positions[0]).normalized;

                // Distribute bones along the direction towards the target
                for (var i = 1; i < _positions.Length; i++)
                    _positions[i] = _positions[i - 1] + dir * _lengths[i - 1];
            }
            else
            {
                for (var iteration = 0; iteration < _iterations; iteration++)
                {
                    for (var i = _positions.Length - 1; i > 0; i--)
                    {
                        if (i == _positions.Length - 1)
                        {
                            // Just set the effector to the target position
                            _positions[i] = target;
                        }
                        else
                        {
                            // Move the current bone to its new position on the line based on its length and the position of the next bone
                            _positions[i] = _positions[i + 1] + (_positions[i] - _positions[i + 1]).normalized * _lengths[i];
                        }
                    }

                    for (var i = 1; i < _positions.Length; i++)
                    {
                        // This time set the current bone's position to the position on the line between itself and the previous bone, taking its length into consideration
                        _positions[i] = _positions[i - 1] + (_positions[i] - _positions[i - 1]).normalized * _lengths[i - 1];
                    }

                    // Stop iterating if we are close enough according to the accuracy value
                    var sqrDistance = (_positions[^1] - target).sqrMagnitude;
                    if (sqrDistance < _accuracy.Sqr())
                        break;
                }
            }

            // We are only interested in the bones between the first and last one
            for (var i = 1; i < _positions.Length - 1; i++) 
            {
                var projectionPlane = new Plane(_positions[i + 1] - _positions[i - 1], _positions[i - 1]);
                var projectedBonePosition = projectionPlane.ClosestPointOnPlane(_positions[i]);
                var projectedControl = projectionPlane.ClosestPointOnPlane(pole);
                var angleOnPlane = Vector3.SignedAngle(projectedBonePosition - _positions[i - 1], projectedControl - _positions[i - 1], projectionPlane.normal);
                _positions[i] = Quaternion.AngleAxis(angleOnPlane, projectionPlane.normal) * (_positions[i] - _positions[i - 1]) + _positions[i - 1];
            }
        
            for (var i = 0; i < _bones.Length; i++)
                _bones[i].position = _positions[i];
        
            for (var i = 0; i < _positions.Length; i++)
            {
                if (i == _positions.Length - 1)
                    continue;
                _bones[i].rotation = Quaternion.FromToRotation(_startDirections[i], _positions[i + 1] - _positions[i]) * _startRotations[i];
            }
        }
    }
}