﻿using System;
using UnityEditor;
using UnityEngine;

namespace Moonin.PoseAnimations.Constraints
{
    [RequireComponent(typeof(Bone))]
    public abstract class BoneConstraint : MonoBehaviour
    {
        public Bone Bone { get; private set; }

        protected virtual void OnValidate()
        {
            Bone = GetComponent<Bone>();
        }

        public abstract bool Process();
        public virtual void LateProcess()
        {
        }

        public virtual void OnInspectorGUI()
        {
            GUILayout.Label(GetType().Name, EditorStyles.boldLabel);
        }

        public virtual Action GetCopyAction(BoneConstraint source, bool mirror)
        {
            return null;
        }
    }
}