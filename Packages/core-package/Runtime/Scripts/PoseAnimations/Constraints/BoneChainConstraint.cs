﻿using System.Collections.Generic;

namespace Moonin.PoseAnimations.Constraints
{
    public abstract class BoneChainConstraint : BoneConstraint
    {
        public abstract List<Bone> Chain { get; }
    }
}