﻿using System;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace.Extensions;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Moonin.PoseAnimations.Constraints
{
    public class LimbBoneConstraint : BoneChainConstraint
    {
        [SerializeField] private bool _flipJoint;
        [SerializeField] private bool _keepEndRotation;
        [SerializeField] private bool _attached;
        
        [SerializeField, HideInInspector] private LimbIKSolver _solver;
        [SerializeField, HideInInspector] private Object[] _bonesTransforms;
        [SerializeField, HideInInspector] private List<Bone> _chain;
        [SerializeField, HideInInspector] private Quaternion _axisToForwardRotation;
        [SerializeField, HideInInspector] private Quaternion _homeRotation;
        [SerializeField, HideInInspector] private Vector3 _targetPos;
        [SerializeField, HideInInspector] private Vector3 _polePos;
        [SerializeField, HideInInspector] private Vector3 _defaultTarget;
        [SerializeField, HideInInspector] private Mode _mode;
        [SerializeField, HideInInspector] private PoleMode _poleMode;
        [SerializeField, ReadOnly] private float _poleAngle;
        [SerializeField, HideInInspector] private float _defaultPoleAngle;
        [SerializeField, HideInInspector] private bool _initialized;

        public override List<Bone> Chain => _chain;

        private Bone Root => Bone.Parent;
        private Bone Joint => Bone;
        private Bone End => Bone.Child;

        protected override void OnValidate()
        {
            base.OnValidate();
            if (!_initialized) Reinitialize();

            Bone.Armature.Reinitialized -= Reinitialize;
            Bone.Armature.Reinitialized += Reinitialize;
            Bone.Armature.DefaultPoseSet -= OnDefaultPoseSet;
            Bone.Armature.DefaultPoseSet += OnDefaultPoseSet;
            Bone.Armature.PoseReset -= OnPoseReset;
            Bone.Armature.PoseReset += OnPoseReset;
        }
        private void OnPoseReset()
        {
            //TODO: Remove _defaultTarget and _defaultPoleAngle and use OnPoseSet()
            _targetPos = _defaultTarget;
            _poleAngle = _defaultPoleAngle;
            Reinitialize();
        }
        private void OnDefaultPoseSet()
        {
            _defaultTarget = _targetPos;
            _defaultPoleAngle = _poleAngle;
        }
        private void Reinitialize()
        {
            if (Root == null || End == null)
            {
                Debug.LogError(
                    $"[{name}] {nameof(LimbBoneConstraint)} requires the Bone to have a Child and a Parent Bones");
                DestroyImmediate(this);
                return;
            }

            _chain = new List<Bone> {Root, Joint, End};
            _targetPos = _defaultTarget = End.Position;

            var axis = CalculateAxis();
            var forward = CalculateForward();

            _polePos = Joint.Position + (_flipJoint ? -1 : 1) * forward;
            _axisToForwardRotation = Quaternion.FromToRotation(axis, forward);
            _homeRotation = Quaternion.LookRotation(forward, axis);

            _solver = new LimbIKSolver(Root.transform, Joint.transform, End.transform, _homeRotation);

            _bonesTransforms = new Object[]
            {
                Root.transform,
                Joint.transform,
                End.transform
            };

            _initialized = true;

            //TODO: 2. Add switch to use PositionHandle for a pole instead of angle (anchored in Root)
        }
        private Vector3 CalculateAxis()
        {
            return (Root.Position - End.Position).normalized;
        }
        private Vector3 CalculateForward()
        {
            var axis = CalculateAxis();
            var forward = Vector3.ProjectOnPlane(Joint.Position - Root.Position, axis).normalized;
            var right = Vector3.Cross(forward == Vector3.zero ? Vector3.forward : forward, axis);
            return Vector3.Cross(axis, right);
        }

        public override bool Process()
        {
            if (!_initialized) return false;

            if (!ProcessTarget(out var target) &
                !ProcessPole(out var poleAngle, out var pole))
                return false;

            Undo.RecordObjects(_bonesTransforms.Append(this).ToArray(), $"Modified bone {Bone.name} IK chain");
            _targetPos = target;
            _polePos = pole;
            _poleAngle = poleAngle;
            _solver.Update(_targetPos, _polePos, _flipJoint, _keepEndRotation);
            return true;
        }
        private bool ProcessPole(out float poleAngle, out Vector3 pole)
        {
            return _poleMode switch
            {
                PoleMode.Angle => ProcessPoleAngleMode(out poleAngle, out pole),
                PoleMode.Position => ProcessPolePositionMode(out poleAngle, out pole),
                _ => throw new UnknownEnumException<PoleMode>(_poleMode)
            };
        }
        private bool ProcessPolePositionMode(out float poleAngle, out Vector3 pole)
        {
            poleAngle = _poleAngle;
            EditorGUI.BeginChangeCheck();
            pole = _mode switch
            {
                Mode.FreeMove => Handles.FreeMoveHandle(_polePos, Quaternion.identity,
                    .3f * HandleUtility.GetHandleSize(_polePos), Vector3.zero, Handles.SphereHandleCap),
                Mode.PerAxisMove => Handles.PositionHandle(_polePos, Quaternion.identity),
                _ => _polePos
            };
            return EditorGUI.EndChangeCheck();
        }
        private bool ProcessPoleAngleMode(out float poleAngle, out Vector3 pole)
        {
            var axis = CalculateAxis();
            var forward = Vector3.ProjectOnPlane(_axisToForwardRotation * axis, axis).normalized;
            var position = Vector3.Lerp(Root.Position, End.Position, .5f);

            EditorGUI.BeginChangeCheck();
            poleAngle = PoleAngleHandle(position, axis, _poleAngle);
            pole = position + Quaternion.AngleAxis(_poleAngle, axis) * forward;

            Debug.DrawRay(position, Quaternion.AngleAxis(poleAngle, axis) * forward, Handles.zAxisColor);
            return EditorGUI.EndChangeCheck();
        }
        private bool ProcessTarget(out Vector3 target)
        {
            EditorGUI.BeginChangeCheck();
            target = _mode switch
            {
                Mode.FreeMove => Handles.FreeMoveHandle(_targetPos, Quaternion.identity,
                    .3f * HandleUtility.GetHandleSize(_targetPos), Vector3.zero, Handles.SphereHandleCap),
                Mode.PerAxisMove => Handles.PositionHandle(_targetPos, Quaternion.identity),
                _ => _targetPos
            };

            return EditorGUI.EndChangeCheck();
        }
        private float PoleAngleHandle(Vector3 position, Vector3 axis, float angle)
        {
            Handles.color = Handles.zAxisColor;
            var size = HandleUtility.GetHandleSize(position);
            var rotation = Handles.Disc(
                Quaternion.AngleAxis(angle, axis),
                position,
                axis,
                size,
                true,
                0);
            rotation.ToAngleAxis(out angle, out var newAxis);

            angle *= Vector3.Dot(axis, newAxis).Sign();
            angle = angle > 180 ? angle - 360 : angle < -180 ? angle + 360 : angle;

            return angle;
        }

        public override void LateProcess()
        {
            if (_attached || !Root.transform.hasChanged) return;
            _solver.Update(_targetPos, _polePos, _flipJoint, _keepEndRotation);
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUILayout.BeginHorizontal();
            foreach (Mode mode in Enum.GetValues(typeof(Mode)))
                GUILayoutExtensions.Button(mode.ToString(), () => ChangeMode(mode), _mode != mode);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            foreach (PoleMode poleMode in Enum.GetValues(typeof(PoleMode)))
                GUILayoutExtensions.Button(poleMode.ToString(), () => ChangePoleMode(poleMode), _poleMode != poleMode);
            GUILayout.EndHorizontal();
        }
        private void ChangeMode(Mode mode) => _mode = mode;
        private void ChangePoleMode(PoleMode poleMode) => _poleMode = poleMode;

        public override Action GetCopyAction(BoneConstraint source, bool mirror)
        {
            if (source is not LimbBoneConstraint limbBoneConstraint)
                return null;

            var polePos = mirror ? MirrorPosition(limbBoneConstraint._polePos) : limbBoneConstraint._polePos;
            var targetPos = mirror ? MirrorPosition(limbBoneConstraint._targetPos) : limbBoneConstraint._targetPos;

            return () =>
            {
                _polePos = polePos;
                _targetPos = targetPos;
            };

            Vector3 MirrorPosition(Vector3 position)
            {
                var rootPosition = Bone.Armature.transform.position;
                position -= rootPosition;
                position.x *= -1;
                return position + rootPosition;
            }
        }

        private enum Mode
        {
            FreeMove,
            PerAxisMove,
        }

        private enum PoleMode
        {
            Angle,
            Position,
        }
    }
}