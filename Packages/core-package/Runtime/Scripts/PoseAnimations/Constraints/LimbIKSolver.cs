﻿using System;
using UnityEngine;

namespace Moonin.PoseAnimations.Constraints
{
    [Serializable]
    public class LimbIKSolver
    {
        [SerializeField, HideInInspector] private Transform _root;
        [SerializeField, HideInInspector] private Transform _joint;
        [SerializeField, HideInInspector] private Transform _end;
        [SerializeField, HideInInspector] private float _rootLength;
        [SerializeField, HideInInspector] private float _jointLength;
        [SerializeField, HideInInspector] private Quaternion _rootHomeRotation;
        [SerializeField, HideInInspector] private Quaternion _jointHomeRotation;

        public LimbIKSolver(Transform root, Transform joint, Transform end, Quaternion homeRotation)
        {
            _root = root;
            _joint = joint;
            _end = end;

            _rootLength = Vector3.Distance(_root.position, _joint.position);
            _jointLength = Vector3.Distance(_joint.position, _end.position);

            _rootHomeRotation = _root.localRotation * Quaternion.Inverse(homeRotation);
            _jointHomeRotation = _joint.localRotation;
        }

        public void Update(Vector3 target, Vector3 pole, bool flipJoint, bool keepEndRotation, bool drawGizmos = false)
        {
            var endRotation = _end.rotation;
            var root = _root.position;
            var planeUp = (root - target).normalized;
            var planeRight = (flipJoint ? -1 : 1) * Vector3.ProjectOnPlane(pole - target, planeUp).normalized;

            var planeTarget = ToPlaneLocal(target, root, planeUp, planeRight);
            var distanceToTarget = planeTarget.magnitude;
            
            var rootAngle = 0f;
            var jointAngle = 0f;
            
            if (distanceToTarget < _rootLength + _jointLength)
            {
                rootAngle = -Mathf.Rad2Deg * Mathf.Acos(
                    (distanceToTarget.Sqr() + _rootLength.Sqr() - _jointLength.Sqr()) /
                    (2 * distanceToTarget * _rootLength));
                jointAngle = 180 - Mathf.Rad2Deg * Mathf.Acos(
                    (_rootLength.Sqr() + _jointLength.Sqr() - distanceToTarget.Sqr()) /
                    (2 * _rootLength * _jointLength));
                
            }

            var axis = flipJoint ? Vector3.left : Vector3.right;
            _root.rotation = Quaternion.LookRotation(planeRight, planeUp) * Quaternion.AngleAxis(rootAngle, axis) * _rootHomeRotation;
            _joint.localRotation = Quaternion.AngleAxis(jointAngle, Quaternion.Inverse(_rootHomeRotation) * axis) * _jointHomeRotation;
            if (keepEndRotation) _end.rotation = endRotation;
            
            if (drawGizmos)
            {
                Debug.DrawRay(target, Vector3.up * .1f, Color.magenta);
                Debug.DrawRay(target, Vector3.down * .1f, Color.magenta);
                Debug.DrawRay(target, Vector3.left * .1f, Color.magenta);
                Debug.DrawRay(target, Vector3.right * .1f, Color.magenta);
                Debug.DrawRay(target, Vector3.forward * .1f, Color.magenta);
                Debug.DrawRay(target, Vector3.back * .1f, Color.magenta);

                Debug.DrawRay(pole, Vector3.up * .1f, Color.cyan);
                Debug.DrawRay(pole, Vector3.down * .1f, Color.cyan);
                Debug.DrawRay(pole, Vector3.left * .1f, Color.cyan);
                Debug.DrawRay(pole, Vector3.right * .1f, Color.cyan);
                Debug.DrawRay(pole, Vector3.forward * .1f, Color.cyan);
                Debug.DrawRay(pole, Vector3.back * .1f, Color.cyan);
                
                Debug.DrawRay(root, planeUp, Color.green);
                Debug.DrawRay(root, planeRight, Color.red);
                
                Debug.DrawLine(root, target, distanceToTarget < _rootLength + _jointLength ? Color.cyan : Color.yellow);
            }
        }
        private Vector2 ToPlaneLocal(Vector3 pointOnPlane, Vector3 origin, Vector3 planeUp, Vector3 planeRight)
        {
            var v = pointOnPlane - origin;
            return new Vector2(Vector3.Dot(planeRight, v), Vector3.Dot(planeUp, v));
        }
    }
}