﻿using UnityEngine;

namespace Moonin.PoseAnimations.Constraints
{
    [ExecuteInEditMode]
    public class IKTest : MonoBehaviour
    {
        [SerializeField] private Transform[] _bones;
        [SerializeField] private Transform _target;
        [SerializeField] private Transform _pole;

        [Button("Reinitialize")]
        private void B_Reinitialize()
        {
            _bones[0].localRotation = Quaternion.identity;
            _bones[0].localPosition = 2 * Vector3.up;
            
            for (var i = 1; i < _bones.Length; i++)
            {
                _bones[i].localPosition = Vector3.down;
                _bones[i].localRotation = Quaternion.identity;
            }

            _pole.localPosition = new Vector3(0, 1, 1);
            _target.localPosition = new Vector3(0, 0, 0);
            
            Initialize();
        }
        
        [SerializeField, HideInInspector] private IKSolver _solver;
        private bool _initialized;
        
        private void Initialize()
        {
            _initialized = true;
            _solver = new IKSolver(_bones);
        }
        
        private void Update()
        {
            if (_solver == null) Initialize();

            _solver.Solve(_target.position, _pole.position); //, _pole.position
        }
    }
}