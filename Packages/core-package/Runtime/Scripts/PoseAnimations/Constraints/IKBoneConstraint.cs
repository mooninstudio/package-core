﻿using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Moonin.PoseAnimations.Constraints
{
    public class IKBoneConstraint : BoneConstraint
    {
        [SerializeField] private int _chainLength;
        [SerializeField] private bool _active = true;

        [SerializeField, ReadOnly] private float _poleAngle;
        [SerializeField, HideInInspector] private float _defaultPoleAngle;
        [SerializeField, HideInInspector] private Quaternion _axisToForwardRotation;
        [SerializeField, HideInInspector] private Vector3 _target;
        [SerializeField, HideInInspector] private Vector3 _defaultTarget;
        [SerializeField, HideInInspector] private bool _initialized;

        [Button("Reinitialize")]
        private void Initialize()
        {
            var chain = Bone.GetBoneChainUp().Take(_chainLength).Reverse().Append(Bone.Child).ToList();
            if (chain.Count < 3) return;
            
            var axis = chain.Last().Position - chain.First().Position;
            var forward = Vector3.ProjectOnPlane(chain[1].Position - chain[0].Position, axis).normalized;
            if (forward == Vector3.zero)
                forward = new Vector3(axis.x, axis.z, -axis.y);
            
            _target = _defaultTarget = Bone.Child.Position;
            _axisToForwardRotation = Quaternion.FromToRotation(axis, forward);
            
            _initialized = true;
        }
        
        public override bool Process()
        {
            if (!_initialized) return false;
            
            var chain = Bone.GetBoneChainUp().Take(_chainLength).Reverse().Append(Bone.Child).ToList();
            if (chain.Count < 3) return false;

            var position = Vector3.Lerp(chain.First().Position, chain.Last().Position, .5f);
            var axis = chain.Last().Position - chain.First().Position;
            var forward = _axisToForwardRotation * axis;
            
            
            var poleDirection = PoleHandle(position, axis, forward, ref _poleAngle);
            _target = Handles.PositionHandle(_target, Quaternion.identity);

            if (!_active) return false;

            var solver = new FABRIKSolver(chain.ToArray());
            solver.Solve(_target, position + poleDirection);
            return true;
        }
        private Vector3 PoleHandle(Vector3 position, Vector3 axis, Vector3 forward, ref float angle)
        {
            Handles.color = Handles.zAxisColor;
            var size = HandleUtility.GetHandleSize(position);
            var rotation = Handles.Disc(
                Quaternion.AngleAxis(angle, axis),
                position,
                axis,
                size,
                true,
                0);
            rotation.ToAngleAxis(out angle, out var newAxis);

            angle *= Vector3.Dot(axis, newAxis).Sign();
            angle = angle > 180 ? angle - 360 : angle < -180 ? angle + 360 : angle;

            var poleDirection = rotation * forward;
            Handles.DrawLine(position, position + poleDirection * size);

            return poleDirection;
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            if (Bone.Child == null)
            {
                Debug.LogError("IK Bone Constraint requires Bone to have a child Bone.");
                Destroy(this);
                return;
            }
            
            if (!_initialized) Initialize();
                
            Bone.Armature.PoseReset -= OnPoseReset;
            Bone.Armature.PoseReset += OnPoseReset;
            Bone.Armature.DefaultPoseSet -= OnDefaultPoseSet;
            Bone.Armature.DefaultPoseSet += OnDefaultPoseSet;
        }
        private void OnPoseReset()
        {
            _target = _defaultTarget;
            _poleAngle = _defaultPoleAngle;
        }
        private void OnDefaultPoseSet()
        {
            _defaultTarget = _target;
            _defaultPoleAngle = _poleAngle;
        }
    }
}