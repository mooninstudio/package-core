﻿using UnityEngine;

namespace Moonin.PoseAnimations.Constraints
{
    [ExecuteInEditMode]
    public class LimbIKTest : MonoBehaviour
    {
        [SerializeField] private Transform _root;
        [SerializeField] private Transform _rootModel;
        [SerializeField] private Transform _joint;
        [SerializeField] private Transform _jointModel;
        [SerializeField] private Transform _end;
        [SerializeField] private Transform _endModel;
        [SerializeField] private Transform _target;
        [SerializeField] private Transform _pole;
        [SerializeField] private bool _flipJoint;
        [SerializeField] private bool _keepEndRotation;
        [Space] 
        [SerializeField] private Vector3 _parentRotation;
        [SerializeField] private Vector3 _rootRotation;
        [SerializeField] private Vector3 _jointRotation;
        [SerializeField] private Vector3 _endRotation;
        [SerializeField] private bool _resetBones;
        [SerializeField] private bool _active;

        private LimbIKSolver _solver;

        [Button]
        private void ResetBones()
        {
            transform.rotation = Quaternion.Euler(_parentRotation);
                
            _root.localPosition = Vector3.zero;
            _root.localRotation = Quaternion.Euler(_rootRotation);
            _rootModel.localRotation = Quaternion.Inverse(Quaternion.Euler(_rootRotation));
                
            _joint.localPosition = Quaternion.Inverse(Quaternion.Euler(_rootRotation)) * Vector3.down;
            _joint.localRotation = Quaternion.Inverse(Quaternion.Euler(_rootRotation)) * Quaternion.Euler(_jointRotation);
            _jointModel.localRotation = Quaternion.Inverse(Quaternion.Euler(_jointRotation));
                
            _end.localPosition = Quaternion.Inverse(Quaternion.Euler(_jointRotation)) * Vector3.down;
            _end.localRotation = Quaternion.Inverse(Quaternion.Euler(_jointRotation)) * Quaternion.Euler(_endRotation);
            _endModel.localRotation = Quaternion.Inverse(Quaternion.Euler(_endRotation));
                
            _target.position = _end.position;
            _target.localRotation = Quaternion.identity;
                
            _pole.localPosition = Vector3.forward + Vector3.down;
        }
        
        [Button]
        private void Reinitialize()
        {
            if (_resetBones) ResetBones();
            var up = (_root.position - _end.position).normalized;
            var forward = Vector3.ProjectOnPlane(_pole.position - _joint.position, up).normalized;
            _solver = new LimbIKSolver(_root, _joint, _end, Quaternion.LookRotation(forward, up));
        }
        
        private void Update()
        {
            if (_solver == null)
                Reinitialize();
            if (_active)
                _solver.Update(_target.position, _pole.position, _flipJoint, _keepEndRotation);
        }
    }
}