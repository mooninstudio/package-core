﻿using System;
using System.Linq;
using UnityEngine;

namespace Moonin.PoseAnimations
{
    [Serializable]
    public struct Keyframe
    {
        [SerializeField] private string _name;
        [SerializeField] private Pose[] _poses;
        [Button]
        public void Test() => Debug.Log("Test");
        
        public bool IsEmpty => Poses.IsNullOrEmpty();
        public Pose[] Poses => _poses;

        public static Keyframe TakeSnapshot(Armature target)
        {
            return new Keyframe()
            {
                _poses = target.Bones.Select(x => x.Pose).ToArray()
            };
        }
    }
}