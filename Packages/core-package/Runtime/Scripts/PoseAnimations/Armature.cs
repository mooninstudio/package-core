﻿using UnityEngine;

namespace Moonin.PoseAnimations
{
    public partial class Armature : MonoBehaviour
    {
        public Bone[] Bones { get; private set; }

        private void OnValidate()
        {
            Bones = GetComponentsInChildren<Bone>();
        }

        public bool TryGetBoneIndex(Bone bone, out int index)
        {
            for (var i = 0; i < Bones.Length; i++)
                if (Bones[i] == bone)
                {
                    index = i;
                    return true;
                };

            index = -1;
            return false;
        }
    }
}