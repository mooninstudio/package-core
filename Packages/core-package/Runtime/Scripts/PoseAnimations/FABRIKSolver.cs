﻿using System;
using System.Linq;
using UnityEngine;

namespace Moonin.PoseAnimations
{
    [Serializable]
    public class FABRIKSolver
    {
        [SerializeField, HideInInspector] private Bone[] _chain;
        [SerializeField, HideInInspector] private Bone _root;
        [SerializeField, HideInInspector] private Transform _rootTransform;
        [SerializeField, HideInInspector] private float[] _boneLengths;
        [SerializeField, HideInInspector] private float _chainLengthSqr;
        [SerializeField, HideInInspector] private float _toleranceSqr;
        [SerializeField, HideInInspector] private int _maxIterations;

        [SerializeField, HideInInspector] private Vector3[] _defaultPositions;
        [SerializeField, HideInInspector] private Quaternion[] _defaultRotations;

        public FABRIKSolver(Bone[] chain, float tolerance = 0.01f, int maxIterations = 100)
        {
            _chain = chain;
            _root = _chain.First();
            _rootTransform = _root.transform;
            _boneLengths = _chain.Select(bone => bone.Length).ToArray();
            _chainLengthSqr = _boneLengths.Sum().Sqr();
            _toleranceSqr = tolerance * tolerance;
            _maxIterations = maxIterations;
            
            Debug.Log(_boneLengths.Sum());
        }

        public void Solve(Vector3 target, Vector3 pole)
        {
            var toTarget = target - _root.Position;
            Vector3[] bonePositions;
            if (toTarget.sqrMagnitude > _chainLengthSqr)
            {
                var dir = toTarget.normalized;
                bonePositions = new Vector3[_chain.Length];
                bonePositions[0] = _root.Position;
                for (var i = 1; i < _chain.Length; i++)
                    bonePositions[i] = bonePositions[i - 1] + dir * _boneLengths[i - 1];
            }
            else
            {
                bonePositions = _chain.Select(bone => bone.Position).ToArray();
                for (var iteration = 0; iteration < _maxIterations; iteration++)
                {
                    for (var i = bonePositions.Length - 1; i > 0; i--)
                    {
                        if (i == bonePositions.Length - 1)
                            bonePositions[i] = target;
                        else
                            bonePositions[i] = bonePositions[i + 1] + (bonePositions[i] - bonePositions[i + 1]).normalized * _boneLengths[i];
                    }
                    
                    for (var i = 1; i < bonePositions.Length; i++)
                        bonePositions[i] = bonePositions[i - 1] + (bonePositions[i] - bonePositions[i - 1]).normalized * _boneLengths[i - 1];
                    
                    if ((bonePositions[^1] - target).sqrMagnitude < _toleranceSqr)
                        break;
                }
            }

            for (var i = 1; i < bonePositions.Length - 1; i++)
            {
                var plane = new Plane(bonePositions[i + 1] - bonePositions[i - 1], bonePositions[i - 1]);
                var projectedPole = plane.ClosestPointOnPlane(pole);
                var projectedBone = plane.ClosestPointOnPlane(bonePositions[i]);
                var angle = Vector3.SignedAngle(projectedBone - bonePositions[i - 1], projectedPole - bonePositions[i - 1], plane.normal);
                bonePositions[i] = Quaternion.AngleAxis(angle, plane.normal) * (bonePositions[i] - bonePositions[i - 1]) + bonePositions[i - 1];
            }
            
            
            bonePositions.Pairwise().ForEach(x => Debug.DrawLine(x.Item1, x.Item2));

            for (var i = 0; i < bonePositions.Length - 1; i++)
            {
                var target1 = bonePositions[i];
                var target2 = bonePositions[i + i];
                var bone1 = _chain[i];
                var bone2 = _chain[i + 1];
                
                var from = (bone2.Position - bone1.Position).normalized;
                var to = (target2 - target1).normalized;
            
                _chain[i].transform.rotation = Quaternion.FromToRotation(from, to);
            }
        }
    }
}
