﻿using System.Linq;

namespace Moonin.PoseAnimations
{
    public static class KeyframeFunc
    {
        public static void Apply(this Keyframe pose, Armature target)
        {
            pose.Poses
                .Zip(target.Bones, (pose, bone) => (pose, bone))
                .ForEach(pair => pair.bone.SetPose(pair.pose));
        }
        
        public static void LerpApply(Armature target, Keyframe from, Keyframe to, float t)
        {
            from.Poses
                .Zip(to.Poses, (fromPose, toPose) => (fromPose, toPose))
                .Select(x => PoseFunc.Lerp(x.fromPose, x.toPose, t))
                .Zip(target.Bones, (pose, bone) => (pose, bone))
                .ForEach(pair => pair.bone.SetPose(pair.pose));
        }
    }
}