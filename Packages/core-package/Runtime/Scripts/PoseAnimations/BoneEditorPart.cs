﻿using UnityEditor;

namespace Moonin.PoseAnimations
{
    public partial class Bone
    {
        [Button("Armature")]
        private void B_SelectArmature()
        {
            var armature = GetComponentInParent<Armature>();
            if (armature == null) return;
            
            Selection.activeGameObject = armature.gameObject;
        }
    }
}