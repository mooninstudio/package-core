﻿// ReSharper disable InconsistentNaming

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Moonin.PoseAnimations
{
    public partial class Armature
    {
        public event Action PoseReset;
        public event Action Reinitialized;
        public event Action DefaultPoseSet;

        [field: SerializeField] public float BoneWidth { get; private set; }
        [field: SerializeField] public Color DefaultBoneColor { get; private set; } = new(1, 1, 1, .7f);
        [field: SerializeField] public Color SelectedBoneColor { get; private set; } = new(1, 1, .6f, .7f);
        [field: SerializeField] public Color ActiveBoneColor { get; private set; } = new(0, 1, 0, .7f);

        [field: SerializeField, HideInInspector]
        public List<int> SelectedBones { get; } = new();
        public int ActiveBoneIndex => SelectedBones.Any() ? SelectedBones.Last() : -1;
        
        [SerializeField, HideInInspector] private Mesh _boneMesh;
        [SerializeField, HideInInspector] private Keyframe _defaultPose;
        [SerializeField, HideInInspector] private Vector3[] _boneMeshVertices = new Vector3[16];

        public void E_Reinitialize()
        {
            Bones = GetComponentsInChildren<Bone>();
            Reinitialized?.Invoke();
        }
        public void E_UpdateDefaultPose()
        {
            _defaultPose = Keyframe.TakeSnapshot(this);
            DefaultPoseSet?.Invoke();
        }
        public void E_ResetPose()
        {
            _defaultPose.Apply(this);
            PoseReset?.Invoke();
        }
        public bool E_CanResetPose => !_defaultPose.IsEmpty;
        public Mesh E_GetBoneMesh(float length)
        {
            if (_boneMesh == null)
                _boneMesh = GetBoneMesh();

            var diagLength = BoneWidth / length * Mathf.Sqrt(2);
            var diagA = (Vector3.forward + Vector3.right).normalized * .5f;
            var diagB = (Vector3.forward - Vector3.right).normalized * .5f;
            _boneMeshVertices[04] = diagB * diagLength; // 1
            _boneMeshVertices[05] = diagA * diagLength; // 2
            _boneMeshVertices[06] = diagA * diagLength; // 2
            _boneMeshVertices[07] = -diagB * diagLength; // 3
            _boneMeshVertices[08] = -diagB * diagLength; // 3
            _boneMeshVertices[09] = -diagA * diagLength; // 4
            _boneMeshVertices[10] = -diagA * diagLength; // 4
            _boneMeshVertices[11] = diagB * diagLength; // 1

            _boneMeshVertices[12] = diagB * diagLength; // 1
            _boneMeshVertices[13] = diagA * diagLength; // 2
            _boneMeshVertices[14] = -diagB * diagLength; // 3
            _boneMeshVertices[15] = -diagA * diagLength; // 4
            _boneMesh.vertices = _boneMeshVertices;

            return _boneMesh;
        }
        private Mesh GetBoneMesh()
        {
            var diagLength = BoneWidth * Mathf.Sqrt(2);
            var diagA = (Vector3.forward + Vector3.right).normalized * .5f;
            var diagB = (Vector3.forward - Vector3.right).normalized * .5f;

            _boneMeshVertices[00] = Vector3.up;
            _boneMeshVertices[01] = Vector3.up;
            _boneMeshVertices[02] = Vector3.up;
            _boneMeshVertices[03] = Vector3.up;

            _boneMeshVertices[04] = diagB * diagLength; // 1
            _boneMeshVertices[05] = diagA * diagLength; // 2
            _boneMeshVertices[06] = diagA * diagLength; // 2
            _boneMeshVertices[07] = -diagB * diagLength; // 3
            _boneMeshVertices[08] = -diagB * diagLength; // 3
            _boneMeshVertices[09] = -diagA * diagLength; // 4
            _boneMeshVertices[10] = -diagA * diagLength; // 4
            _boneMeshVertices[11] = diagB * diagLength; // 1

            _boneMeshVertices[12] = diagB * diagLength; // 1
            _boneMeshVertices[13] = diagA * diagLength; // 2
            _boneMeshVertices[14] = -diagB * diagLength; // 3
            _boneMeshVertices[15] = -diagA * diagLength; // 4

            var triangles = new int[6 * 3];
            var i = 0;
            triangles[i + 0] = 00;
            triangles[i + 1] = 04;
            triangles[i + 2] = 05;
            i += 3;
            triangles[i + 0] = 01;
            triangles[i + 1] = 06;
            triangles[i + 2] = 07;
            i += 3;
            triangles[i + 0] = 02;
            triangles[i + 1] = 08;
            triangles[i + 2] = 09;
            i += 3;
            triangles[i + 0] = 03;
            triangles[i + 1] = 10;
            triangles[i + 2] = 11;
            i += 3;
            triangles[i + 0] = 12;
            triangles[i + 1] = 14;
            triangles[i + 2] = 13;
            i += 3;
            triangles[i + 0] = 12;
            triangles[i + 1] = 15;
            triangles[i + 2] = 14;

            var mesh = new Mesh
            {
                vertices = _boneMeshVertices,
                triangles = triangles
            };
            mesh.RecalculateNormals();
            return mesh;
        }

        // private void OnDrawGizmos()
        // {
        //     for (var i = 0; i < Bones.Length; i++)
        //     {
        //         var bone = Bones[i];
        //         if (bone.Length == 0) continue;
        //
        //         var trans = bone.transform;
        //         var root = trans.position;
        //         var up = trans.up;
        //         var selected = SelectedBones.Contains(i);
        //         var active = SelectedBones.Any() && SelectedBones.Last() == i;
        //
        //         var forward = trans.forward;
        //         var right = trans.right;
        //         var diagLength = BoneWidth * Mathf.Sqrt(2);
        //         var diagA = (forward + right).normalized * .5f;
        //         var diagB = (forward - right).normalized * .5f;
        //
        //         Gizmos.color =
        //             active ? ActiveBoneColor :
        //             selected ? SelectedBoneColor :
        //             DefaultBoneColor;
        //
        //         if (active)
        //             Gizmos.DrawMesh(
        //                 E_GetBoneMesh(bone.Length),
        //                 bone.transform.position,
        //                 bone.transform.rotation,
        //                 new Vector3(BoneWidth, bone.Length, BoneWidth));
        //         else
        //             Gizmos.DrawWireMesh(
        //                 E_GetBoneMesh(bone.Length),
        //                 bone.transform.position,
        //                 bone.transform.rotation,
        //                 new Vector3(bone.Length, bone.Length, bone.Length));
        //         // Gizmos.DrawLine(root + diagA * diagLength, root - diagA * diagLength);
        //         // Gizmos.DrawLine(root + diagB * diagLength, root - diagB * diagLength);
        //         //
        //         // Gizmos.DrawLine(root + diagA * diagLength, root + diagB * diagLength);
        //         // Gizmos.DrawLine(root + diagB * diagLength, root - diagA * diagLength);
        //         // Gizmos.DrawLine(root - diagA * diagLength, root - diagB * diagLength);
        //         // Gizmos.DrawLine(root - diagB * diagLength, root + diagA * diagLength);
        //         //
        //         // Gizmos.DrawLine(root + diagA * diagLength, root + up * bone.Length);
        //         // Gizmos.DrawLine(root + diagB * diagLength, root + up * bone.Length);
        //         // Gizmos.DrawLine(root - diagA * diagLength, root + up * bone.Length);
        //         // Gizmos.DrawLine(root - diagB * diagLength, root + up * bone.Length);
        //     }
        // }
    }
}