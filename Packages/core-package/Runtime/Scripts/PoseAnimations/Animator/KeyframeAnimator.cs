﻿using UnityEngine;

namespace Moonin.PoseAnimations.Animator
{
    public class KeyframeAnimator : MonoBehaviour
    {
        [field:SerializeField] public Armature Armature { get; private set; }
        [field:SerializeField] public Clip Clip { get; set; }
        [field:SerializeField, Range(0, 1)] public float Time { get; set; }
        [field:SerializeField] public bool Preview { get; private set; }

        public void UpdatePose()
        {
            if (Clip == null) return;
            
            Clip.Sample(Armature, Time);
        }
    }
}