﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Moonin.PoseAnimations
{
    public partial class Bone : MonoBehaviour
    {
        public event Action PoseSet;
        
        public Pose Pose => new(transform.localPosition, transform.localRotation);
        public Vector3 LocalPosition => transform.localPosition;
        public Vector3 Position
        {
            get => transform.position;
            set => transform.position = value;
        }
        public Quaternion Rotation
        {
            get => transform.rotation;
            set => transform.rotation = value;
        }
        public float Length { get; private set; }
        public Bone Child { get; private set; }
        public Bone Parent { get; private set; }
        public Armature Armature { get; private set; }
        
        public void SetPose(Pose pose)
        {
            transform.localPosition = pose.position;
            transform.localRotation = pose.rotation;
            PoseSet?.Invoke();
            // TODO: Subscribe to PoseSet in LimbConstraint to set target and pole
        }

        private void OnValidate()
        {
            UpdateChildBone();
            Armature = GetComponentInParent<Armature>();
        }

        private void UpdateChildBone()
        {
            var trans = transform;
            var root = trans.position;
            var up = trans.up;

            var childBone = default(Bone);
            foreach (Transform child in trans)
                if (child.TryGetComponent(out childBone))
                    break;

            Child = childBone;
            if (childBone != null)
                childBone.Parent = this;
            
            Length = childBone != null
                ? Vector3.Distance(childBone.transform.position, root)
                : 0;
        }

        public IEnumerable<Bone> GetBoneChainUp()
        {
            var bone = this;
            do
            {
                yield return bone;
                bone = bone.Parent;
            } while (bone != null);
        }
        public IEnumerable<Bone> GetBoneChainDown()
        {
            var bone = this;
            do
            {
                yield return bone;
                bone = bone.Child;
            } while (bone != null);
        }
        public IEnumerable<Bone> GetBoneChain()
        {
            var bone = this;
            var parent = bone.Parent;
            while(parent != null)
            {
                bone = parent;
                parent = bone.Parent;
            }

            return bone.GetBoneChainDown();
        }
    }
}