using UnityEngine;

namespace Moonin.Controller2D
{
    public class Controller2D : RaycastController
    {
        [SerializeField] private float maxClimbAngle = 45;
        [SerializeField] private float maxDescendAngle = 40;
        [SerializeField] private bool startFaceDirRight;

        protected ICollisionInfo Collisions => collisions;

        private CollisionInfo collisions;
        private Vector2 playerInput;

        protected override void Awake()
        {
            base.Awake();
            collisions.FaceDirection = startFaceDirRight ? 1 : -1;
        }

        public void Move(Vector2 moveDelta, bool onPlatform)
        {
            Move(moveDelta, Vector2.zero, onPlatform);
        }

        public void Move(Vector2 moveDelta, Vector2 input, bool onPlatform = false)
        {
            UpdateRaycastOrigins();
            collisions.Reset();
            collisions.moveDeltaOld = moveDelta;
            playerInput = input;

            if (moveDelta.x != 0)
                collisions.FaceDirection = (int) moveDelta.x.Sign();

            if (moveDelta.y < 0)
                DescendSlope(ref moveDelta);

            HorizontalCollisions(ref moveDelta);

            if (moveDelta.y != 0)
                VerticalCollisions(ref moveDelta);

            transform.Translate(moveDelta);

            if (onPlatform)
                collisions.Below = true;
        }

        public void SetPosition(Vector2 position)
        {
            transform.position = position;
        }

        private void HorizontalCollisions(ref Vector2 moveDelta)
        {
            var directionX = collisions.FaceDirection;
            var rayLength = moveDelta.x.Abs() + skinWidth;

            if (moveDelta.x.Abs() < skinWidth)
                rayLength = 2 * skinWidth;

            for (int i = 0; i < raycastResolution.x; i++)
            {
                var rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
                rayOrigin += Vector2.up * (verticalRaySpacing * i);
                var hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

                Debug.DrawRay(rayOrigin, Vector2.right * directionX, Color.red);

                if (hit)
                {
                    if (hit.distance == 0)
                        continue;

                    var slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                    if (i == 0 && slopeAngle <= maxClimbAngle)
                    {
                        if (collisions.DescendingSlope)
                        {
                            collisions.DescendingSlope = false;
                            moveDelta = collisions.moveDeltaOld;
                        }

                        var distanceToSlopeStart = 0f;
                        if (slopeAngle != collisions.SlopeAngleOld)
                        {
                            distanceToSlopeStart = hit.distance - skinWidth;
                            moveDelta.x -= distanceToSlopeStart * directionX;
                        }

                        ClimbSlope(ref moveDelta, slopeAngle);
                        moveDelta.x += distanceToSlopeStart * directionX;
                    }

                    if (!collisions.ClimbingSlope || slopeAngle > maxClimbAngle)
                    {
                        moveDelta.x = (hit.distance - skinWidth) * directionX;
                        rayLength = hit.distance;

                        if (collisions.ClimbingSlope)
                        {
                            moveDelta.y = Mathf.Tan(collisions.SlopeAngle * Mathf.Deg2Rad) * moveDelta.x.Abs();
                        }

                        collisions.Left = directionX == -1;
                        collisions.Right = directionX == 1;
                    }
                }
            }
        }

        private void ClimbSlope(ref Vector2 moveDelta, float slopeAngle)
        {
            var moveDistance = moveDelta.x.Abs();
            var climbmoveDeltaY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

            if (moveDelta.y < climbmoveDeltaY)
            {
                moveDelta.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * moveDelta.x.Sign();
                moveDelta.y = climbmoveDeltaY;
                collisions.Below = true;
                collisions.ClimbingSlope = true;
                collisions.SlopeAngle = slopeAngle;
            }
        }

        private void DescendSlope(ref Vector2 moveDelta)
        {
            var directionX = moveDelta.x.Sign();
            var rayOrigin = (directionX == -1) ? raycastOrigins.bottomRight : raycastOrigins.bottomLeft;
            var hit = Physics2D.Raycast(rayOrigin, Vector2.down, Mathf.Infinity, collisionMask);

            if (hit)
            {
                var slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                if (slopeAngle != 0 && slopeAngle <= maxDescendAngle)
                {
                    if (hit.normal.x.Sign() == directionX)
                    {
                        if (hit.distance - skinWidth <= Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * moveDelta.x.Abs())
                        {
                            var moveDistance = moveDelta.x.Abs();
                            var descendmoveDeltaY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
                            moveDelta.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * moveDelta.x.Sign();
                            moveDelta.y -= descendmoveDeltaY;
                            collisions.SlopeAngle = slopeAngle;
                            collisions.Below = true;
                            collisions.DescendingSlope = true;
                        }
                    }
                }
            }
        }

        private void VerticalCollisions(ref Vector2 moveDelta)
        {
            var directionY = moveDelta.y.Sign();
            var rayLength = moveDelta.y.Abs() + skinWidth;

            for (int i = 0; i < raycastResolution.y; i++)
            {
                var rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
                rayOrigin += Vector2.right * (verticalRaySpacing * i + moveDelta.x);
                var hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);

                Debug.DrawRay(rayOrigin, Vector2.up * directionY, Color.red);

                if (hit)
                {
                    if (hit.transform.tag == "Through")
                    {
                        if (directionY == 1 || hit.distance == 0)
                            continue;
                        if (collisions.FallingThroughPlatform)
                            continue;
                        if (playerInput.y.Sign() == -1)
                        {
                            collisions.FallingThroughPlatform = true;
                            Invoke("ResetFallingThroughPlatform", .5f);
                            continue;
                        }
                    }

                    moveDelta.y = (hit.distance - skinWidth) * directionY;
                    rayLength = hit.distance;

                    if (collisions.ClimbingSlope)
                    {
                        moveDelta.x = moveDelta.y / Mathf.Tan(collisions.SlopeAngle * Mathf.Deg2Rad) *
                                      moveDelta.x.Sign();
                    }

                    collisions.Below = directionY == -1;
                    collisions.Above = directionY == 1;
                }
            }

            if (collisions.ClimbingSlope)
            {
                var directionX = moveDelta.x.Sign();
                rayLength = moveDelta.x.Abs() + skinWidth;
                var rayOrigin = ((directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight) +
                                Vector2.up * moveDelta.y;
                var hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);
                if (hit)
                {
                    var slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                    if (slopeAngle != collisions.SlopeAngle)
                    {
                        moveDelta.x = (hit.distance - skinWidth) * directionX;
                        collisions.SlopeAngle = slopeAngle;
                    }
                }
            }
        }

        private void ResetFallingThroughPlatform()
        {
            collisions.FallingThroughPlatform = false;
        }

        private struct CollisionInfo : ICollisionInfo
        {
            public bool Above { get; set; }
            public bool Below { get; set; }
            public bool Left { get; set; }
            public bool Right { get; set; }
            public bool ClimbingSlope { get; set; }
            public bool DescendingSlope { get; set; }
            public float SlopeAngle { get; set; }
            public float SlopeAngleOld { get; set; }
            public Vector2 moveDeltaOld { get; set; }
            public int FaceDirection { get; set; }
            public bool FallingThroughPlatform { get; set; }

            public void Reset()
            {
                Above = false;
                Below = false;
                Left = false;
                Right = false;
                ClimbingSlope = false;
                DescendingSlope = false;
                SlopeAngleOld = SlopeAngle;
                SlopeAngle = 0;
            }
        }

        public interface ICollisionInfo
        {
            bool Above { get; }
            bool Below { get; }
            bool Left { get; }
            bool Right { get; }
            bool ClimbingSlope { get; }
            bool DescendingSlope { get; }
            float SlopeAngle { get; }
            float SlopeAngleOld { get; }
            int FaceDirection { get; }
        }
    }
}