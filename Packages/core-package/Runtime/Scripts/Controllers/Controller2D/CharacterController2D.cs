using UnityEngine;

namespace Moonin.Controller2D
{
	public class CharacterController2D : Controller2D
	{
		[Header("General")] [SerializeField] private float maxJumpHeight = 4f;
		[SerializeField] private float minJumpHeight = 1f;
		[SerializeField] private float timeToJumpApex = .4f;
		[SerializeField] private float moveSpeed = 6f;
		[SerializeField] private float accelerationTimeGrounded = .1f;
		[SerializeField] private float accelerationTimeAirborne = .2f;
		[SerializeField] private float maxFallVelocity = 10f;

		[Header("Wall slide")] [SerializeField]
		private bool wallSlideEnabled;

		[SerializeField, ConditionalField(nameof(wallSlideEnabled))]
		private float wallSlideSpeedMax = 6f;

		[Header("Wall jump")] [SerializeField] private bool wallJumpEnabled;

		[SerializeField, ConditionalField(nameof(wallJumpEnabled))]
		private Vector2 wallJumpClimb;

		[SerializeField, ConditionalField(nameof(wallJumpEnabled))]
		private Vector2 wallJumpOff;

		[SerializeField, ConditionalField(nameof(wallJumpEnabled))]
		private Vector2 wallJumpLeap;

		[SerializeField, ConditionalField(nameof(wallJumpEnabled))]
		private float wallStickTime = .25f;

		private Vector3 velocity;
		private Vector2 directionalInput;
		private float velocityXSmoothing;
		private float timeToWallUnstick;
		private bool wallSliding;
		private int wallDirectionX;

		private float _gravity;
		private float _maxJumpVelocity;
		private float _minJumpVelocity;

		protected override void Awake()
		{
			base.Awake();
			_gravity = -(2 * maxJumpHeight / timeToJumpApex.Sqr());
			_maxJumpVelocity = _gravity.Abs() * timeToJumpApex;
			_minJumpVelocity = (2 * _gravity.Abs() * minJumpHeight).Sqrt();
		}

		public void Tick()
		{
			CalculateVelocity();

			if (wallSlideEnabled)
				HandleWallSliding();

			Move(velocity * Time.deltaTime, directionalInput);

			if (Collisions.Above || Collisions.Below)
				velocity.y = 0;
		}

		public void SetDirectionalInput(Vector2 input)
		{
			directionalInput = input;
		}

		public void OnJumpInputDown()
		{
			if (wallJumpEnabled && wallSliding)
			{
				if (wallDirectionX == directionalInput.x.Sign())
				{
					velocity.x = -wallDirectionX * wallJumpClimb.x;
					velocity.y = wallJumpClimb.y;
				}
				else if (directionalInput.x == 0)
				{
					velocity.x = -wallDirectionX * wallJumpOff.x;
					velocity.y = wallJumpOff.y;
				}
				else
				{
					velocity.x = -wallDirectionX * wallJumpLeap.x;
					velocity.y = wallJumpLeap.y;
				}
			}

			if (Collisions.Below)
			{
				velocity.y = _maxJumpVelocity;
			}
		}

		public void OnJumpInputUp()
		{
			if (velocity.y > _minJumpVelocity)
				velocity.y = _minJumpVelocity;
		}

		private void HandleWallSliding()
		{
			wallDirectionX = (Collisions.Left) ? -1 : 1;
			wallSliding = false;

			if ((Collisions.Left || Collisions.Right) && !Collisions.Below && velocity.y < 0)
			{
				wallSliding = true;
				if (velocity.y < -wallSlideSpeedMax)
					velocity.y = -wallSlideSpeedMax;

				if (timeToWallUnstick > 0)
				{
					velocity.x = 0;
					velocityXSmoothing = 0;

					if (directionalInput.x != wallDirectionX && directionalInput.x != 0)
						timeToWallUnstick -= Time.deltaTime;
					else
						timeToWallUnstick = wallStickTime;
				}
				else
				{
					timeToWallUnstick = wallStickTime;
				}
			}
		}

		private void CalculateVelocity()
		{
			var targetVelocityX = directionalInput.x * moveSpeed;
			velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing,
				Collisions.Below ? accelerationTimeGrounded : accelerationTimeAirborne);
			velocity.y += _gravity * Time.deltaTime;
			velocity.y = velocity.y.Min(-maxFallVelocity);
		}
	}
}