﻿using Moonin.PoseAnimations.Animator;
using DefaultNamespace.Extensions;
using UnityEditor;
using UnityEngine;

namespace Moonin.PoseAnimations.Editor
{
    [CustomEditor(typeof(KeyframeAnimator))]
    public class KeyframeAnimatorEditor : UnityEditor.Editor
    {
        private KeyframeAnimator _animator;
        private KeyframeAnimator Animator => _animator ??= target as KeyframeAnimator;
        private int _stagedToDelete = -1;
        private int _stagedToEditName = -1;

        public override void OnInspectorGUI()
        {
            var prevTime = Animator.Time;
            base.OnInspectorGUI();

            var hasArmature = Animator.Armature != null;
            var hasClip = Animator.Clip != null;

            if (!hasClip) GUILayoutExtensions.Button("New clip", AddNewClip, hasArmature);
            if (hasClip) GUILayoutExtensions.Button("Add keyframe", AddKeyframe, hasArmature);
            if (Animator.Preview) Animator.UpdatePose();

            if (!hasClip) return;
            var clipObject = serializedObject.FindProperty("<Clip>k__BackingField").objectReferenceValue;
            var clipSerializedObject = new SerializedObject(clipObject);
            var clipKeyframesProperty = clipSerializedObject.FindProperty("<Keyframes>k__BackingField");

            for (var i = 0; i < clipKeyframesProperty.arraySize; i++)
            {
                GUILayout.BeginHorizontal();
                GUILayoutExtensions.Button("↑", () => MoveFrameUp(i), i > 0);
                GUILayoutExtensions.Button("↓", () => MoveFrameDown(i), i < clipKeyframesProperty.arraySize - 1);
                GUILayoutExtensions.Button("Go to", () => GoToFrame(i));
                
                //TODO: Add "edit" button for name
                //TODO: Allow animator to affect Armature's bone constraints
                GUI.enabled = _stagedToEditName == i;
                EditorGUILayout.PropertyField(clipKeyframesProperty.GetArrayElementAtIndex(i).FindPropertyRelative("_name"), GUIContent.none);
                GUI.enabled = true;
                
                GUILayout.FlexibleSpace();
                GUILayoutExtensions.ButtonWithConfirmation("Remove",
                    _stagedToDelete == i,
                    () => _stagedToDelete = i,
                    () => RemoveFrame(i),
                    () => _stagedToDelete = -1);
                GUILayout.EndHorizontal();
            }

            clipSerializedObject.ApplyModifiedProperties();

            void MoveFrameUp(int frame)
            {
                clipKeyframesProperty.MoveArrayElement(frame, frame - 1);
            }
            void MoveFrameDown(int frame)
            {
                clipKeyframesProperty.MoveArrayElement(frame, frame + 1);
            }
            void RemoveFrame(int frame)
            {
                clipKeyframesProperty.DeleteArrayElementAtIndex(frame);
                _stagedToDelete = -1;
            }
            void GoToFrame(int frame)
            {
                Animator.Time = Animator.Clip.Keyframes.Count == 1
                    ? 0f
                    : frame / (Animator.Clip.Keyframes.Count - 1f);
            }
        }
        private void AddNewClip()
        {
            var path = EditorUtility.SaveFilePanelInProject("Add new clip", "", "asset", "Test message");
            var clip = ScriptableObject.CreateInstance<Clip>();
            AssetDatabase.CreateAsset(clip, path);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            Animator.Clip = clip;
        }
        private void AddKeyframe()
        {
            Animator.Clip.Keyframes.Add(Keyframe.TakeSnapshot(Animator.Armature));
        }
    }
}