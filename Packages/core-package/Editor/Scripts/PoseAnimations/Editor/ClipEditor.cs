﻿using UnityEditor;

namespace Moonin.PoseAnimations.Editor
{
    [CustomEditor(typeof(Clip))]
    public class ClipEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }
    }
}