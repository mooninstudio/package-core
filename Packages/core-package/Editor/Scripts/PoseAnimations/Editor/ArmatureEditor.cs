﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moonin.PoseAnimations.Constraints;
using DefaultNamespace.Extensions;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Moonin.PoseAnimations.Editor
{
    [CustomEditor(typeof(Armature))]
    public class ArmatureEditor : UnityEditor.Editor
    {
        private Armature _armature;
        private Armature Armature => _armature ??= target as Armature;

        private readonly List<BonePose> _copiedBonePoses = new();

        private bool _updateInitialised;
        private bool _undoPerformed;

        private void OnEnable() => UnityEditor.Tools.hidden = true;
        private void OnDisable() => UnityEditor.Tools.hidden = false;

        public override void OnInspectorGUI()
        {
            GUILayoutExtensions.Button("Reinitialize", Armature.E_Reinitialize);
            GUILayoutExtensions.ButtonWithConfirmation("Update default pose", ref _updateInitialised,
                Armature.E_UpdateDefaultPose, "Update");
            GUILayoutExtensions.Button("Reset pose", ResetPose, Armature.E_CanResetPose);
            GUILayoutExtensions.Button("Mirror whole pose", MirrorPose);
            GUILayout.BeginHorizontal();
            GUILayoutExtensions.Button("Select all", SelectAll);
            GUILayoutExtensions.Button("Copy selected", Copy);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayoutExtensions.Button("Paste", () => Paste(false), _copiedBonePoses.Any());
            GUILayoutExtensions.Button("Paste Mirrored", () => Paste(true), _copiedBonePoses.Any());
            GUILayout.EndHorizontal();
            GUILayoutExtensions.Button("Go to Bone", GoToActiveBone, Armature.SelectedBones.Any());

            base.OnInspectorGUI();

            if (!Armature.SelectedBones.Any()) return;

            var activeBone = Armature.Bones[Armature.SelectedBones.Last()];
            GUILayout.Space(EditorGUIUtility.singleLineHeight);
            GUILayout.Label($"Active bone: {activeBone.name}");
            activeBone.transform.GetComponents<BoneConstraint>()
                .ForEach(constraint =>
                {
                    constraint.OnInspectorGUI();
                    CreateEditor(constraint).OnInspectorGUI();
                });
        }
        private void ResetPose()
        {
            UndoRecordPose("Reset pose.");
            Armature.E_ResetPose();
        }
        private void MirrorPose()
        {
            UndoRecordPose("Mirrored pose.");
            var poses = Armature.Bones.Select((bone, index) => new BonePose {Bone = index, Pose = bone.Pose}).ToList();
            CopyPoses(poses, MirrorBonePoses(poses));
        }
        private void GoToActiveBone()
        {
            var bone = Armature.Bones[Armature.SelectedBones.Last()];
            Selection.activeGameObject = bone.gameObject;
        }
        private void SelectAll()
        {
            Armature.SelectedBones.AddRange(Enumerable.Range(0, Armature.Bones.Length));
        }
        private void Copy()
        {
            _copiedBonePoses.Clear();
            Armature.SelectedBones.ForEach(boneIndex =>
            {
                var bone = Armature.Bones[boneIndex];
                _copiedBonePoses.Add(new BonePose {Bone = boneIndex, Pose = bone.Pose});
            });
        }
        private void Paste(bool mirrored)
        {
            if (!_copiedBonePoses.Any()) return;

            var posesToApply = mirrored
                ? MirrorBonePoses(_copiedBonePoses)
                : _copiedBonePoses;
            
            CopyPoses(_copiedBonePoses, posesToApply);
        }
        private void CopyPoses(IEnumerable<BonePose> source, IEnumerable<BonePose> target)
        {
            target.Zip(source, (target, source) => (target, source))
                .Select(data =>
                {
                    var (target, source) = data;
                    var targetBone = Armature.Bones[target.Bone];
                    var sourceBone = Armature.Bones[source.Bone];
                    
                    Action action = () => targetBone.SetPose(target.Pose);
                    if (sourceBone.TryGetComponent<BoneConstraint>(out var sourceConstraint) &&
                        targetBone.TryGetComponent<BoneConstraint>(out var targetConstraint))
                        action += targetConstraint.GetCopyAction(sourceConstraint, true);
                    return action;
                })
                .Aggregate((agg, action) => agg + action)
                .Invoke();
        }

        public void OnSceneGUI()
        {
            var activeBone = Armature.ActiveBoneIndex;
            for (var i = 0; i < Armature.Bones.Length; i++)
            {
                var bone = Armature.Bones[i];
                if (bone.Length == 0) continue;

                var selected = Armature.SelectedBones.Contains(i);
                var active = activeBone == i;

                Handles.color =
                    active ? Armature.ActiveBoneColor :
                    selected ? Armature.SelectedBoneColor :
                    Armature.DefaultBoneColor;

                var boneTransform = bone.transform;
                var position = boneTransform.position;
                var rotation = boneTransform.rotation;

                if (Handles.Button(position, rotation, bone.Length, bone.Length,
                        GetMeshCapFunction(Armature.E_GetBoneMesh(bone.Length))))
                {
                    if (!Event.current.shift)
                        Armature.SelectedBones.Clear();
                    Armature.SelectedBones.Add(i);

                    BoneChainConstraint chain = null;
                    if (bone.GetBoneChain().Any(x => x.TryGetComponent(out chain)))
                    {
                        chain.Chain.ForEach(b =>
                        {
                            if (!Armature.TryGetBoneIndex(b, out var index)) return;
                            Armature.SelectedBones.Remove(x => x == index);
                            Armature.SelectedBones.Add(index);
                        });

                        if (Armature.TryGetBoneIndex(chain.Bone, out var index))
                        {
                            Armature.SelectedBones.Remove(x => x == index);
                            Armature.SelectedBones.Add(index);
                        }
                    }
                }

                if (!active) continue;

                var dirty = bone.TryGetComponent<BoneConstraint>(out var constraint)
                    ? constraint.Process()
                    : DefaultBoneHandle(bone);

                if (dirty)
                    Repaint();
            }
            
            foreach (var bone in Armature.Bones)
            {
                if (!bone.TryGetComponent<BoneConstraint>(out var constraint))
                    continue;
                
                constraint.LateProcess();
            }

            HandleClearSelection();
            HandleCopy();
            HandlePaste();

            if (activeBone != Armature.ActiveBoneIndex)
                Repaint();
        }
        private bool DefaultBoneHandle(Bone bone)
        {
            var boneTransform = bone.transform;
            var position = boneTransform.position;
            var rotation = boneTransform.rotation;
            var scale = boneTransform.localScale;

            EditorGUI.BeginChangeCheck();

            Handles.TransformHandle(ref position, ref rotation, ref scale);

            if (!EditorGUI.EndChangeCheck()) return false;

            Undo.RecordObject(boneTransform, $"Modified bone {bone.name} transform");
            boneTransform.position = position;
            boneTransform.rotation = rotation;
            boneTransform.localScale = scale;
            return true;
        }
        private void HandleClearSelection()
        {
            if (Event.current.keyCode != KeyCode.Escape) return;

            Armature.SelectedBones.Clear();
        }
        private void HandleCopy()
        {
            if (!Event.current.control || Event.current.keyCode != KeyCode.C || !Armature.SelectedBones.Any()) return;
            Copy();
        }
        private void HandlePaste()
        {
            if (!Event.current.control || Event.current.keyCode != KeyCode.V) return;
            Paste(Event.current.shift);
        }

        private IEnumerable<BonePose> MirrorBonePoses(IEnumerable<BonePose> bonePoses)
        {
            return bonePoses.Select(bonePose =>
            {
                var bone = Armature.Bones[bonePose.Bone];
                var mirrorBoneName = MirrorBoneName(bone.name);

                if (mirrorBoneName != bone.name)
                    bonePose.Bone = FindBoneIndex(mirrorBoneName);

                bonePose.Pose = bonePose.Pose.Mirror();

                return bonePose;
            });

            int FindBoneIndex(string boneName)
            {
                for (var i = 0; i < Armature.Bones.Length; i++)
                    if (Armature.Bones[i].name == boneName)
                        return i;

                return -1;
            }
        }
        private string MirrorBoneName(string boneName)
        {
            if (boneName.EndsWith(".L")) return $"{boneName.TrimEnd('L')}R";
            if (boneName.EndsWith(".R")) return $"{boneName.TrimEnd('R')}L";
            return boneName;
        }
        private void UndoRecordPose(string message = "Pose changed.")
        {
            Undo.RecordObjects(Armature.Bones.Select(bone => bone.transform as Object).ToArray(), message);
        }

        private static Handles.CapFunction GetMeshCapFunction(Mesh mesh)
        {
            return (controlID, position, rotation, size, eventType) =>
                MeshHandleCap(controlID, mesh, position, rotation, size, eventType, DistanceToBone);
        }
        private static float DistanceToBone(Vector3 position, Quaternion rotation, float size)
        {
            return HandleUtility.DistanceToLine(position, position + rotation * Vector3.up * size) * .7f;
        }
        private static void MeshHandleCap(int controlID, Mesh mesh, Vector3 position, Quaternion rotation, float size,
            EventType eventType, Func<Vector3, Quaternion, float, float> pointerDistanceFunction = null)
        {
            switch (eventType)
            {
                case EventType.MouseMove:
                case EventType.Layout:
                    HandleUtility.AddControl(controlID, pointerDistanceFunction?.Invoke(position, rotation, size)
                                                        ?? HandleUtility.DistanceToCircle(position, size));
                    break;
                case EventType.Repaint:
                    Graphics.DrawMeshNow(mesh, StartCapDraw(position, rotation, size));
                    break;
            }
        }
        private static Matrix4x4 StartCapDraw(Vector3 position, Quaternion rotation, float size)
        {
            Shader.SetGlobalColor("_HandleColor", Handles.color);
            Shader.SetGlobalFloat("_HandleSize", size);
            var matrix4X4 = Handles.matrix * Matrix4x4.TRS(position, rotation, Vector3.one);
            Shader.SetGlobalMatrix("_ObjectToWorld", matrix4X4);
            HandleUtility.handleMaterial.SetFloat("_HandleZTest", (float) Handles.zTest);
            HandleUtility.handleMaterial.SetPass(0);
            return matrix4X4;
        }
        private static bool ButtonWithConfirmation(string title, ref bool isOpen, string confirmText = "Confirm",
            string cancelText = "Cancel")
        {
            if (!isOpen && GUILayout.Button(title))
            {
                isOpen = true;
                return false;
            }

            var result = default(bool);
            GUILayout.BeginHorizontal();
            if (isOpen && GUILayout.Button(confirmText))
            {
                isOpen = false;
                result = true;
            }

            if (isOpen && GUILayout.Button(cancelText))
            {
                isOpen = false;
                result = false;
            }

            GUILayout.EndHorizontal();

            return result;
        }

        private struct BonePose
        {
            public int Bone { get; set; }
            public Pose Pose { get; set; }
        }
    }
}