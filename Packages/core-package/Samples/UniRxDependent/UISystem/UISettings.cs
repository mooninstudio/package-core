using UnityEngine;

namespace Moonin.UISystem
{   
    [CreateAssetMenu(fileName = "UISettings", menuName = "Custom/UISettings")]
    public class UISettings : ScriptableObject 
    {
        [SerializeField] private float clickTime;

        public float ClickTime => clickTime;
    }
}