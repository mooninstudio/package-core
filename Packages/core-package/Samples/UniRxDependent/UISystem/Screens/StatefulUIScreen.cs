using Moonin.State;

namespace Moonin.UISystem
{
    public abstract class UIScreenBase<TState> : UIScreenBase, IUIScreen<TState> where TState : BaseState
    {
        protected TState state;

        public virtual void OnAfterSetup(TState state)
        {
            base.OnAfterSetup();
        }

        public virtual void Setup(TState state)
        {
            base.Setup();
            this.state = state;
        }
        public override void Setup() => throw new System.Exception($"{gameObject.name} is a stateful screen. State should be provided via Setup({nameof(TState)})");
    }

    public interface IUIScreen<TState> where TState : BaseState
    {
        void Setup(TState state);
        void OnAfterSetup(TState state);
    }
}