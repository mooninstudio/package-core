using System;
using UniRx;
using UnityEngine;

namespace Moonin.UISystem
{   
    [System.Serializable]
    public class ClickableWidgetComponent
    {
        [SerializeField] private UIClickHandler clickHandler;

        public IObservable<Unit> PressedStream => clickHandler.ClickStream.Where(_ => clickable);

        private bool clickable = true;

        public void SetClickable(bool clickable)
        {
            this.clickable = clickable;
        }
    }
}