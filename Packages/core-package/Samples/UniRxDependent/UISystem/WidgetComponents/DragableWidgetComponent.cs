using System;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Moonin.UISystem
{   
    public class DragableWidgetComponent : IDragable
    {
        [SerializeField] private RectTransform rectTransform;

        public IObservable<Vector2> DragStream => dragSubject.AsObservable();
        public IObservable<Vector2> DragStartStream => dragStartSubject.AsObservable();
        public IObservable<Vector2> DragEndStream => dragEndSubject.AsObservable();

        private Subject<Vector2> dragSubject = new Subject<Vector2>();
        private Subject<Vector2> dragStartSubject = new Subject<Vector2>();
        private Subject<Vector2> dragEndSubject = new Subject<Vector2>();

        public void OnDrag(PointerEventData eventData) => dragSubject.OnNext(GetNormalizedPointerPosition(eventData.position));
        public void OnPointerDown(PointerEventData eventData) => dragStartSubject.OnNext(GetNormalizedPointerPosition(eventData.position));
        public void OnPointerUp(PointerEventData eventData) => dragEndSubject.OnNext(GetNormalizedPointerPosition(eventData.position));

        private Vector2 GetNormalizedPointerPosition(Vector2 screenPosition)
        {
            return RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, screenPosition, null, out Vector2 localPos) ?
                Rect.PointToNormalized(rectTransform.rect, localPos) : Vector2.zero;
        }
    }

    public interface IDragable : IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        IObservable<Vector2> DragStream { get; }
        IObservable<Vector2> DragStartStream { get; }
        IObservable<Vector2> DragEndStream { get; }
    }
}