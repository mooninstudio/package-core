﻿using System;
using UniRx;
using UnityEngine;

namespace Moonin.UISystem
{   
	[Serializable]
	public class NavigatableWidgetComponent
	{
		[SerializeField] private NavigationHandler navigationHandler;

		public IObservable<Unit> SelectedStream => navigationHandler.SelectedStream;
		public IObservable<Unit> DeselectedStream => navigationHandler.DeselectedStream;
	}
}
