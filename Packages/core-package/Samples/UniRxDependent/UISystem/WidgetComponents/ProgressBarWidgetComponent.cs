using UnityEngine;
using System;
using UnityEngine.UI;

namespace Moonin.UISystem
{   
    [Serializable]
    public class ProgressBarWidgetComponent
    {
        [SerializeField] private Image fillImage;

        public void SetProgress(float value)
        {
            fillImage.fillAmount = value;
        }
    }
}