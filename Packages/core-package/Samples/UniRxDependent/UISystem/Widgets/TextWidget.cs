using UnityEngine;

namespace Moonin.UISystem
{   
    public class TextWidget : Widget
    {
        [SerializeField] private TextWidgetComponent textComponent;

        public void SetText(string text) => textComponent.SetText(text);
        public void SetTextColor(Color color) => textComponent.SetTextColor(color);
    }
}