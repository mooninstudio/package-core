using System;
using UniRx;
using UnityEngine;

namespace Moonin.UISystem
{   
	public class ButtonWidget : Widget
	{
		[SerializeField] private ClickableWidgetComponent btnCmp;

		public IObservable<Unit> PressedStream => btnCmp.PressedStream;
	}
}