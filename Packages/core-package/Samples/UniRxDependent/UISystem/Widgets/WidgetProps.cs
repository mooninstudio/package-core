using System;
using UniRx;
namespace Moonin.UISystem
{   
    public class WidgetProps<T> : IWidgetProp<T>
    {
        public IObservable<T> StateUpdateStream => stateUpdateSubject.AsObservable();
        private Subject<T> stateUpdateSubject = new Subject<T>();
    }

    public interface IWidgetProp<T>
    {
        IObservable<T> StateUpdateStream { get; }
    }
}