using UnityEngine;

namespace Moonin.UISystem
{   
    public class ImageWidget : Widget
    {
        [SerializeField] private ImageWidgetComponent image;

        public void SetSprite(Sprite sprite) => image.SetSprite(sprite);
        public void SetColor(Color color) => image.SetColor(color);
    }
}