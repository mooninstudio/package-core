using System;
using UniRx;
using UnityEngine;

namespace Moonin.UISystem
{   
    public abstract class ScrollItemBase<TData, TId> : Widget, IScrollItem<TId>
        where TData : IWithId<TId>
    {
        public Guid ItemId { get; private set; } = Guid.NewGuid();
        public TId DataId { get; private set; }

        public IObservable<Vector2> PositionChangedStream => positionChangedSubject.AsObservable();
        private Subject<Vector2> positionChangedSubject = new Subject<Vector2>();

        public virtual void Move(Vector2 delta)
        {
            RectTransform.anchorMin += delta;
            RectTransform.anchorMax += delta;
            positionChangedSubject.OnNext(RectTransform.anchorMin);
        }

        public virtual void SetPosition(Vector2 position)
        {
            RectTransform.anchorMin = position;
            RectTransform.anchorMax = position;
            positionChangedSubject.OnNext(RectTransform.anchorMin);
        }

        public virtual void LoadData(TData data)
        {
            DataId = data.Id;
        }
    }
}