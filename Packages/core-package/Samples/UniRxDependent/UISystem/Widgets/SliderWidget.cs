using System;
using UnityEngine;
using UniRx;
using UnityEngine.EventSystems;

namespace Moonin.UISystem
{   
    public class SliderWidget : GroupWidget
    {
        [SerializeField] private ConfigurableOverlay input;
        [SerializeField] private bool vertical;

        public IObservable<float> ValueStream => Observable.Merge(
            input.PointerDragStream,
            input.PointerDownStream,
            input.PointerUpStream
        ).Select(ValueFromPointer);

        private readonly UIEventType eventMask = 
            UIEventType.Drag
            | UIEventType.PointerDown
            | UIEventType.PointerEnter
            | UIEventType.PointerExit
            | UIEventType.PointerUp;

        protected override void OnAwake()
        {
            base.OnAwake();
            input.Configure(eventMask);
        }

        private float ValueFromPointer(PointerEventData pointerData)
        {
            var value = input.RectTransform.ScreenPointToLocalNormalised(pointerData.position);
            return vertical ? value.y : value.x;
        }
    }
}