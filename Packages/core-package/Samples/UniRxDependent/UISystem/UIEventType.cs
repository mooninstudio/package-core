using System;

namespace Moonin.UISystem
{   
	[Flags] public enum UIEventType
	{
		PointerClick	= 1 << 0,
		PointerDown		= 1 << 1,
		PointerUp		= 1 << 2,
		Drag			= 1 << 3,
		PointerEnter	= 1 << 4,
		PointerExit 	= 1 << 5,
	};
}