using UnityEngine;
using UnityEngine.UI;

namespace Moonin.UISystem
{
    [RequireComponent(typeof(GridLayoutGroup)),RequireComponent(typeof(RectTransform)), ExecuteAlways]
    public class GridLayoutGroupDriver : MonoBehaviour
    {
        [SerializeField] private int childCount;
        [SerializeField] private bool equalSpacing;

        private RectTransform rectTransform;
        private GridLayoutGroup layout;

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
            layout = GetComponent<GridLayoutGroup>();
        }

        private void Update()
        {
            int rows, columns;
            switch (layout.constraint)
            {
                case GridLayoutGroup.Constraint.FixedColumnCount:
                    columns = layout.constraintCount;
                    rows = Mathf.CeilToInt(childCount / (float)columns);
                    break;
                case GridLayoutGroup.Constraint.FixedRowCount:
                    rows = layout.constraintCount;
                    columns = Mathf.CeilToInt(childCount / (float)rows);
                    break;
                case GridLayoutGroup.Constraint.Flexible:
                default: return;
            }
            layout.spacing = CalculateSpacing(rows, columns);
            
        }
        private Vector2 CalculateSpacing(int rows, int columns)
        {
            var spacing = rectTransform.rect.size;
            spacing -= new Vector2(
                layout.padding.left + layout.padding.right,
                layout.padding.bottom + layout.padding.top);
            spacing -= new Vector2(
                layout.cellSize.x * columns,
                layout.cellSize.y * rows);
            spacing.x /= (columns - 1);
            spacing.y /= (rows - 1);

            if(equalSpacing)
                spacing = Vector2.one * Mathf.Min(spacing.x, spacing.y);

            return spacing;
        }
    }
}