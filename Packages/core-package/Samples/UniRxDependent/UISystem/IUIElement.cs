﻿using UnityEngine;

namespace Moonin.UISystem
{
    public interface IUIElement
    {
        RectTransform RectTransform { get; }
    }
}