using UnityEngine;
using UnityEngine.UI;

namespace Moonin.UISystem
{   
    [RequireComponent(typeof(Graphic)), DisallowMultipleComponent]
    public class ConfigurableOverlay : OverlayBase
    {
        protected override UIEventType InterceptMask => interceptMask;
        protected override UIEventType ConsumeMask => consumeMask;

        private UIEventType consumeMask = 0;
        private UIEventType interceptMask = 0;

        public void Configure(UIEventType consumeMask, UIEventType interceptMask)
        {
            this.consumeMask = consumeMask;
            this.interceptMask = interceptMask;
        }
        
        public void Configure(UIEventType interactMask)
        {
            this.consumeMask = interactMask;
            this.interceptMask = interactMask;
        }
    }
}
