﻿using System;
using System.Collections;
using UniRx;
using ObservableExtensions = UniRx.ObservableExtensions;

namespace Samples.UniRxDependent.Extensions
{
    public static class EnumeratorExt
    {
        public static IDisposable Subscribe(this IEnumerator enumerator, Action onEmit = null)
        {
            return ObservableExtensions.Subscribe(enumerator.ToObservable(), _ => onEmit?.Invoke());
        }
    }
}