﻿using System;
using UniRx;

namespace Moonin.Input
{
	public interface IObservableControl<TValue>
	{
		IObservable<TValue> ValueStream { get; }
		TValue Value { get; }
	}
	public abstract class ObservableControl<TValue> : IObservableControl<TValue>
	{
		public IObservable<TValue> ValueStream => valueSubject.AsObservable();
		public TValue Value { get; private set; }

		private Subject<TValue> valueSubject = new Subject<TValue>();

		public void OnNext(TValue value)
		{
			Value = value;
			valueSubject.OnNext(value);
		}
	}
}