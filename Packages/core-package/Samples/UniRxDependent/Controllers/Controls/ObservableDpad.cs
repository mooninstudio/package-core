﻿using System;
using UniRx;
using UnityEngine;

namespace Moonin.Input
{
	public interface IObservableDpad : IObservableControl<Vector2>
	{
		IObservable<Unit> UpPressedStream { get; }
		IObservable<Unit> DownPressedStream { get; }
		IObservable<Unit> LeftPressedStream { get; }
		IObservable<Unit> RightPressedStream { get; }
	}
	public class ObservableDpad : ObservableControl<Vector2>, IObservableDpad
	{
		public IObservable<Unit> DownPressedStream => ValueStream.Pairwise((a, b) => a.y > -Threshold && b.y < -Threshold).Where(x => x).AsUnitObservable();
		public IObservable<Unit> UpPressedStream => ValueStream.Pairwise((a, b) => a.y < Threshold && b.y > Threshold).Where(x => x).AsUnitObservable();
		public IObservable<Unit> LeftPressedStream => ValueStream.Pairwise((a, b) => a.x > -Threshold && b.x < -Threshold).Where(x => x).AsUnitObservable();
		public IObservable<Unit> RightPressedStream => ValueStream.Pairwise((a, b) => a.x < Threshold && b.x > Threshold).Where(x => x).AsUnitObservable();
		private const float Threshold = .5f;
	}
}