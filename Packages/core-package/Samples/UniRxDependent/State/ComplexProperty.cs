﻿using System;
using UniRx;

namespace Moonin.State
{
    public class ComplexProperty<T> : Property<T>
    {
        public ComplexProperty(T defaultValue) : base(defaultValue)
        {
        }

        public void Set(Action<T> propertyModifier)
        {
            propertyModifier(Value);
            ChangeValue(Value);
        }

        public IObservable<TValue> ObserveValue<TValue>(Func<T, TValue> valueSelector)
        {
            return this.Select(valueSelector);
        }
    }
}