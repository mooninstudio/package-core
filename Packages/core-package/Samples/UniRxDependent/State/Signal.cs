using System;
using UniRx;

namespace Moonin.State
{
    public class Signal : SignalBase
    {
        private Subject<Unit> subject = new Subject<Unit>();
        
        public void Trigger()
        {
            subject.OnNext();
        }

        public override IDisposable Subscribe(IObserver<Unit> observer) => subject.AsObservable().Subscribe(observer);
    }
}