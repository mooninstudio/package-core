using System;
using UniRx;

namespace Moonin.State
{
    public abstract class SignalBase : IObservable<Unit>
    {
        public abstract IDisposable Subscribe(IObserver<Unit> observer);
    }
}