﻿using System;
using UniRx;

namespace Moonin.State
{
    public abstract class ChangablePropertyBase<T> : PropertyBase<T>
    {
        protected Subject<T> changeSubject = new Subject<T>();

        public ChangablePropertyBase(T defaults = default)
        {
            Value = defaults;
        }

        public ChangablePropertyBase(Func<T> getDefaultValue)
        {
            Value = getDefaultValue();
        }

        protected void ChangeValue(T newValue)
        {
            Value = newValue;
            changeSubject.OnNext(newValue);
        }

        public override IDisposable Subscribe(IObserver<T> observer) => changeSubject.Subscribe(observer);
    }
}