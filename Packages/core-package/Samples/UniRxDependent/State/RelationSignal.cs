using System;
using UniRx;

namespace Moonin.State
{
    public class RelationSignal : SignalBase
    {
        private IObservable<Unit> relation;
        
        public RelationSignal(IObservable<Unit> relation)
        {
            this.relation = relation;
        }

        public override IDisposable Subscribe(IObserver<Unit> observer) => relation.Subscribe(observer);
    }
}